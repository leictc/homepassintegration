package com.propic.homepassintegration;


import com.propic.homepassintegration.response.HomepassData;
import com.propic.homepassintegration.service.HomepassAPIService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationContextLoader;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Optional;

import static org.junit.Assert.*;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=HomepassIntegrationApplication.class, loader= SpringApplicationContextLoader.class)
public class HomepassAPIServiceTest {
    @Autowired
    public HomepassAPIService homepassAPIService;

    private final String secret ="sv4dMBArt3eaVhqe87QVfqQez3xTVncNy";
    private final String officeId="5cac3f4dc1c2c51ba347e001";
    private final String sinceDatetime ="2019-02-01T01:24:42Z";

    @Test
    public void testRetrievingHomepassData(){
        Optional<HomepassData> homepassDataOptional = homepassAPIService.retrieveHomepassData(officeId, secret, sinceDatetime);
        assertTrue(homepassDataOptional.isPresent());
        assertNull(homepassDataOptional.get().getOfficeId());
        assertFalse(homepassDataOptional.get().getData().getItems().isEmpty());
    }
    @Test
    public void testRetrievingHomepassDataWithFutureDatetime(){
        String futureDatetime ="2199-02-01T01:24:42Z";
        Optional<HomepassData> homepassDataOptional = homepassAPIService.retrieveHomepassData(officeId, secret, futureDatetime);
        assertFalse(homepassDataOptional.isPresent());
    }

    @Test
    public void testRetrievingHomepassDataWithInvalidOfficeId(){
        String invalidOfficeId ="sfdsadf23sdfasd";
        Optional<HomepassData> homepassDataOptional = homepassAPIService.retrieveHomepassData(invalidOfficeId, secret, sinceDatetime);
        assertFalse(homepassDataOptional.isPresent());
    }

    @Test
    public void testRetrievingHomepassDataWithInvalidSecret(){
        String invalidSecret="dfsafads2rTwqe45FDsdaf";
        Optional<HomepassData> homepassDataOptional = homepassAPIService.retrieveHomepassData(officeId, invalidSecret, sinceDatetime);
        assertFalse(homepassDataOptional.isPresent());
    }


    @Test
    public void testRetrievingHomepassDataWithoutSecret(){
        String tempSecret = null;
        Optional<HomepassData> homepassDataOptional = homepassAPIService.retrieveHomepassData(officeId, tempSecret, sinceDatetime);
        assertFalse(homepassDataOptional.isPresent());
        tempSecret ="";
        homepassDataOptional = homepassAPIService.retrieveHomepassData(officeId, tempSecret, sinceDatetime);
        assertFalse(homepassDataOptional.isPresent());
    }

    @Test
    public void testRetrievingHomepassDataWithoutOfficeId(){
        String tempOfficeId = null;
        Optional<HomepassData> homepassDataOptional = homepassAPIService.retrieveHomepassData(tempOfficeId, secret, sinceDatetime);
        assertFalse(homepassDataOptional.isPresent());
        tempOfficeId = "";
        homepassDataOptional = homepassAPIService.retrieveHomepassData(tempOfficeId, secret, sinceDatetime);
        assertFalse(homepassDataOptional.isPresent());

    }

    @Test
    public void testRetrievingHomepassDataWithoutDatetime(){
        String tempSinceDatetime = null;
        Optional<HomepassData> homepassDataOptional = homepassAPIService.retrieveHomepassData(officeId, secret, tempSinceDatetime);
        assertFalse(homepassDataOptional.isPresent());
        tempSinceDatetime = "";
        homepassDataOptional = homepassAPIService.retrieveHomepassData(officeId, secret, tempSinceDatetime);
        assertFalse(homepassDataOptional.isPresent());

    }
}
