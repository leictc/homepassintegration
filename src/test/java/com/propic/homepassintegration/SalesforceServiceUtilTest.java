package com.propic.homepassintegration;


import com.propic.homepassintegration.entity.Activity;
import com.propic.homepassintegration.entity.ClientManagementType;
import com.propic.homepassintegration.entity.OrganisationHomepassData;
import com.propic.homepassintegration.response.*;
import com.propic.homepassintegration.util.SalesforceServiceUtil;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class SalesforceServiceUtilTest {
    private final String orgId ="00DO00000055v14MAA";

    private final String officeId ="5630be58204b89c90b2034ea";
    private final String secret ="sv6666dMBArt3eaVhqe87QVfqQez3xTVncNy";
    private final String sinceDatetime ="2019-02-01T01:24:42Z";
    private final String namespace ="prpt__";

    private final String AGENT_EMAIL ="steve@gmail.com";
    private final String CHECKIN ="checkin";
    private final String AGENT_ID ="urn:homepass:agent:5cae7d2f19e06c35be89cc8f";

    private final String TARGET_EMAIL ="target@gmail.com";
    private final String TARGET_FIRSTNAME ="Micheal";
    private final String TARGET_LASTNAME ="OWEN";
    private final String TARGET_LANDLINE ="0245692162";
    private final String TARGET_MOBILE ="+61495746333";





    @Test
    public void testParseHomeData() {
        final String activityTime ="2019-07-09T00:06:45.323Z";
        final String activityId = "urn:homepass:activity:5d23da957c5d9016ec0d680f";


        HomepassData homepassData = new HomepassData();
        homepassData.setOfficeId(officeId);
        homepassData.setType(ClientManagementType.RESIDENTIAL);
        Item item = new Item();
        item.setId(activityId);
        item.setType("http://activitystrea.ms/2.0/Activity");
        item.setPublished(activityTime);
        item.setVerb(CHECKIN);
        item.setActor(createTestActor());
        item.setTarget(createTestTarget());
        List<Item> items = new ArrayList<>();
        items.add(item);
        Data data = new Data();
        data.setItems(items);
        homepassData.setData(data);
        List<HomepassData> homepassDatas = new ArrayList<>();
        homepassDatas.add(homepassData);
        OrganisationHomepassData organisationHomepassData = new OrganisationHomepassData(orgId, homepassDatas);
        List<OrganisationHomepassData> organisationHomepassDataLis = new ArrayList<>();
        organisationHomepassDataLis.add(organisationHomepassData);

        Map<String, List<Activity>> orgIdActivitiesMap = SalesforceServiceUtil.parseHomepassData(organisationHomepassDataLis);
        assertFalse(orgIdActivitiesMap.isEmpty());
        List<Activity> activities = orgIdActivitiesMap.get(orgId);
        assertFalse(activities.isEmpty());
        assertEquals(1, activities.size());
        assertEquals(activityTime, activities.get(0).getActivityTime());
        assertEquals(AGENT_EMAIL, activities.get(0).getAgentEmail());
        assertEquals(activityId, activities.get(0).getHomepassActivityId());
        assertEquals(AGENT_ID, activities.get(0).getHomepassAgentId());
        assertEquals(ClientManagementType.RESIDENTIAL, activities.get(0).getType());
        assertEquals(CHECKIN, activities.get(0).getVerb());
        assertEquals(officeId, activities.get(0).getOfficeId());
        assertEquals(1, activities.get(0).getClientList().size());
        assertEquals(TARGET_EMAIL, activities.get(0).getClientList().get(0).getEmail());
        assertEquals(TARGET_FIRSTNAME, activities.get(0).getClientList().get(0).getFirstName());
        assertEquals(TARGET_LASTNAME, activities.get(0).getClientList().get(0).getLastname());
        assertEquals(TARGET_MOBILE, activities.get(0).getClientList().get(0).getMobile());
        assertEquals(TARGET_LANDLINE, activities.get(0).getClientList().get(0).getLandline());


    }

    private Address createTestAddress(){
        Address address = new Address();
        address.setCountryName("australia");
        address.setLocality("sydney");
        address.setPostalCode("2000");
        address.setRegion("");
        address.setStreetAddress("");
        return address;
    }

    private PersonDetail createTestPersonDetailForActor(){
        PersonDetail personDetail = new PersonDetail();
        personDetail.setEmail(AGENT_EMAIL);
        personDetail.setFirstName("steve");
        personDetail.setLastName("jobs");
        personDetail.setLandline("0245692162");
        personDetail.setMobile("+61412098745");
        return personDetail;
    }

    private Person createTestActor(){
        Person actor = new Person();
        actor.setId(AGENT_ID);
        actor.setType("urn:homepass:types:agent");
        actor.setPersonDetail(createTestPersonDetailForActor());
        actor.setAddress(createTestAddress());
        return actor;
    }

    private Person createTestTarget(){
        Person target = new Person();
        target.setId("urn:homepass:contact:5cc2b7db0dde280f82e57815");
        target.setType("urn:homepass:types:contact");
        target.setPersonDetail(createTestPersonDetailForTarget());
        return target;
    }
    private PersonDetail createTestPersonDetailForTarget(){
        PersonDetail personDetail = new PersonDetail();
        personDetail.setEmail(TARGET_EMAIL);
        personDetail.setFirstName(TARGET_FIRSTNAME);
        personDetail.setLastName(TARGET_LASTNAME);
        personDetail.setLandline(TARGET_LANDLINE);
        personDetail.setMobile(TARGET_MOBILE);
        return personDetail;
    }



}
