package com.propic.homepassintegration;

import com.propic.homepassintegration.entity.*;
import com.propic.homepassintegration.response.HomepassData;
import com.propic.homepassintegration.scheduler.HomepassIntegrationScheduler;
import com.propic.homepassintegration.service.DBService;
import com.propic.homepassintegration.service.HomepassAPISecretService;
import com.propic.homepassintegration.service.HomepassAPIService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.SpringApplicationContextLoader;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.*;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class HomepassIntegrationSchedulerTest {
    @InjectMocks
    private HomepassIntegrationScheduler homepassIntegrationScheduler = new HomepassIntegrationScheduler();

    @Mock
    private HomepassAPISecretService homepassAPISecretService;

    @Mock
    private DBService dbService;

    @Mock
    private HomepassAPIService homepassAPIService;

    private final String orgId ="00DO00000055v14MAA";
    private final String officeId ="5630be58204b89c90b2034ea";
    private final String secret ="sv6666dMBArt3eaVhqe87QVfqQez3xTVncNy";
    private final String sinceDatetime ="2019-02-01T01:24:42Z";
    private final String namespace ="prpt__";

    @Test
    public void test() {

        List<HomepassAPISecret> homepassAPISecrets = new ArrayList<>();
        HomepassAPISecret homepassAPISecret = new HomepassAPISecret();
        homepassAPISecret.setOrgId(orgId);
        homepassAPISecret.setSecret(secret);
        homepassAPISecrets.add(homepassAPISecret);
        when(homepassAPISecretService.findAllAPISecrets()).thenReturn(homepassAPISecrets);

        List<OfficeSynchroniseDatetime> officeSynchroniseDatetimes = new ArrayList<>();
        OfficeSynchroniseDatetime officeSynchroniseDatetime = new OfficeSynchroniseDatetime();
        OfficeTimeId officeTimeId = new OfficeTimeId(orgId, officeId);
        officeSynchroniseDatetime.setId(officeTimeId);
        officeSynchroniseDatetime.setLastSynchroniseDatetime(sinceDatetime);
        officeSynchroniseDatetimes.add(officeSynchroniseDatetime);
        when(dbService.findAllOfficeSynchroniseDatetimes()).thenReturn(officeSynchroniseDatetimes);

        List<HomepassIntegrationRequest> homepassIntegrationRequests = new ArrayList<>();
        HomepassIntegrationRequest homepassIntegrationRequest = new HomepassIntegrationRequest();
        homepassIntegrationRequest.setSecret(secret);
        homepassIntegrationRequest.setNamespace(namespace);
        homepassIntegrationRequest.setOrgId(orgId);
        List<IntegrationOffice> integrationOffices = new ArrayList<>();
        IntegrationOffice integrationOffice = new IntegrationOffice();
        integrationOffice.setProject(true);
        integrationOffice.setOfficeId(officeId);
        integrationOffice.setRequest(homepassIntegrationRequest);
        integrationOffices.add(integrationOffice);
        homepassIntegrationRequest.setIntegrationOffices(integrationOffices);
        homepassIntegrationRequests.add(homepassIntegrationRequest);

        when(dbService.findAllRequests()).thenReturn(homepassIntegrationRequests);
        HomepassData homepassData = new HomepassData();
        homepassData.setCode(12);
        homepassData.setType(ClientManagementType.PROJECT);
        Optional<HomepassData> homepassDataOptional = Optional.of(homepassData);

        //when(homepassAPIService.retrieveHomepassData(any(),any(),any())).thenReturn(homepassDataOptional);
        when(homepassAPIService.retrieveHomepassData(officeId,secret,sinceDatetime)).thenReturn(homepassDataOptional);

        Optional<OfficeSynchroniseDatetime> optionalOfficeSynchroniseDatetime = Optional.of(officeSynchroniseDatetime);

        when(dbService.getOfficeSynchroniseDatetime(any())).thenReturn(Optional.empty());// to do , maybe not use any() here

        try {
            homepassIntegrationScheduler.run();
        }
        catch(Exception e){
            fail("Should not have thrown any exception");
        }


    }


    @Test
    public void testRetrieveHomepassData() {

        List<HomepassIntegrationRequest> homepassIntegrationRequests = new ArrayList<>();
        HomepassIntegrationRequest homepassIntegrationRequest = new HomepassIntegrationRequest();
        homepassIntegrationRequest.setSecret(secret);
        homepassIntegrationRequest.setNamespace(namespace);
        homepassIntegrationRequest.setOrgId(orgId);
        List<IntegrationOffice> integrationOffices = new ArrayList<>();
        IntegrationOffice integrationOffice = new IntegrationOffice();
        integrationOffice.setProject(true);
        integrationOffice.setOfficeId(officeId);
        integrationOffice.setRequest(homepassIntegrationRequest);
        integrationOffices.add(integrationOffice);
        homepassIntegrationRequest.setIntegrationOffices(integrationOffices);
        homepassIntegrationRequests.add(homepassIntegrationRequest);

        Map<String, String> orgSecretMap = new HashMap<>();
        orgSecretMap.put(orgId, secret);

        Map<String, String> officeDatetimeMap = new HashMap<>();
        officeDatetimeMap.put(officeId, sinceDatetime);

        HomepassData homepassData = new HomepassData();
        homepassData.setCode(12);
        homepassData.setType(ClientManagementType.PROJECT);
        Optional<HomepassData> homepassDataOptional = Optional.of(homepassData);

        //when(homepassAPIService.retrieveHomepassData(any(),any(),any())).thenReturn(homepassDataOptional);
        when(homepassAPIService.retrieveHomepassData(officeId,secret,sinceDatetime)).thenReturn(homepassDataOptional);

        List<OrganisationHomepassData> organisationHomepassDataList = homepassIntegrationScheduler.retrieveHomepassData(homepassIntegrationRequests, orgSecretMap, officeDatetimeMap);
        assertFalse(organisationHomepassDataList.isEmpty());
        assertEquals(1,organisationHomepassDataList.size());
        assertEquals(orgId,organisationHomepassDataList.get(0).getOrgId());
        assertFalse(organisationHomepassDataList.get(0).getHomepassDatas().isEmpty());
        assertEquals(1, organisationHomepassDataList.get(0).getHomepassDatas().size());
        assertEquals(officeId, organisationHomepassDataList.get(0).getHomepassDatas().get(0).getOfficeId());
        assertEquals(ClientManagementType.PROJECT, organisationHomepassDataList.get(0).getHomepassDatas().get(0).getType());

        //

        HomepassIntegrationRequest secondHomepassIntegrationRequest = new HomepassIntegrationRequest();
        final String secondOrgId ="second org id";
        secondHomepassIntegrationRequest.setSecret(secret);
        secondHomepassIntegrationRequest.setNamespace(namespace);
        secondHomepassIntegrationRequest.setOrgId(secondOrgId);
        integrationOffices = new ArrayList<>();
        integrationOffice = new IntegrationOffice();
        integrationOffice.setProject(true);
        final String secondOfficeId ="second office id";
        integrationOffice.setOfficeId(secondOfficeId);
        integrationOffice.setRequest(secondHomepassIntegrationRequest);
        integrationOffices.add(integrationOffice);
        secondHomepassIntegrationRequest.setIntegrationOffices(integrationOffices);
        homepassIntegrationRequests.add(secondHomepassIntegrationRequest);


        homepassData = new HomepassData();
        homepassData.setCode(44);
        homepassData.setType(ClientManagementType.RESIDENTIAL);
        homepassDataOptional = Optional.of(homepassData);
        orgSecretMap.put(secondOrgId, secret);
        officeDatetimeMap.put(secondOfficeId, sinceDatetime);

        //when(homepassAPIService.retrieveHomepassData(any(),any(),any())).thenReturn(homepassDataOptional);
        when(homepassAPIService.retrieveHomepassData(secondOfficeId,secret,sinceDatetime)).thenReturn(homepassDataOptional);

        organisationHomepassDataList = homepassIntegrationScheduler.retrieveHomepassData(homepassIntegrationRequests, orgSecretMap, officeDatetimeMap);
        assertFalse(organisationHomepassDataList.isEmpty());

        assertEquals(2,organisationHomepassDataList.size());
        assertEquals(orgId,organisationHomepassDataList.get(0).getOrgId());
        assertEquals(secondOrgId,organisationHomepassDataList.get(1).getOrgId());
        assertFalse(organisationHomepassDataList.get(0).getHomepassDatas().isEmpty());
        assertEquals(1, organisationHomepassDataList.get(0).getHomepassDatas().size());
        assertEquals(officeId, organisationHomepassDataList.get(0).getHomepassDatas().get(0).getOfficeId());
        assertEquals(ClientManagementType.PROJECT, organisationHomepassDataList.get(0).getHomepassDatas().get(0).getType());
        assertFalse(organisationHomepassDataList.get(1).getHomepassDatas().isEmpty());
        assertEquals(1, organisationHomepassDataList.get(1).getHomepassDatas().size());
        assertEquals(secondOfficeId, organisationHomepassDataList.get(1).getHomepassDatas().get(0).getOfficeId());
        assertEquals(ClientManagementType.RESIDENTIAL, organisationHomepassDataList.get(1).getHomepassDatas().get(0).getType());






    }

}
