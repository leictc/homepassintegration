package com.propic.homepassintegration;


import com.propic.homepassintegration.entity.HomepassAPISecret;
import com.propic.homepassintegration.service.HomepassAPISecretService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationContextLoader;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=HomepassIntegrationApplication.class, loader= SpringApplicationContextLoader.class)
public class HomepassAPISecretServiceTest {


    @Autowired
    private HomepassAPISecretService service;

    @Test
    public void test(){
        final String orgId ="00DO00000055v14MAA";
        final String apiSecret ="sv4dMBArt3eaVhqe87QVfqQez3xTVncNy";

        service.saveAPISecret(orgId, apiSecret);
        Optional<HomepassAPISecret> homepassAPISecretOptional = service.retrieveAPISecret(orgId);
        assertTrue(homepassAPISecretOptional.isPresent());

        assertEquals(orgId, homepassAPISecretOptional.get().getOrgId());
        assertEquals(apiSecret, homepassAPISecretOptional.get().getSecret());
    }
}
