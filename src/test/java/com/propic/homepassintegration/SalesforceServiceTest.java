package com.propic.homepassintegration;


import com.propic.homepassintegration.entity.*;
import com.propic.homepassintegration.service.SalesforceService;
import com.sforce.soap.partner.PartnerConnection;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationContextLoader;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=HomepassIntegrationApplication.class, loader= SpringApplicationContextLoader.class)
public class SalesforceServiceTest {
    private final String orgId ="00DO000000531JP";//"00Dp00000001P1E";// "00DO00000055v14MAA";

    @Autowired
    public SalesforceService salesforceService;

    @Test
    public void test(){
        Optional<PartnerConnection> optionalPartnerConnection = salesforceService.establishConnection(orgId);

        Assert.assertTrue( optionalPartnerConnection.isPresent());
        final List<ClientManagement> clientManagements = new ArrayList<>();
        ClientManagement clientManagement = new ClientManagement();
        Account account = new Account();
        account.setFirstname("Steve");
        account.setLastname("Jobs");
        account.setEmail("jobs@test.com");
        account.setLandline("0243561792");
        account.setMobile("0409272182");
        account.setRecordTypeId("01228000000LLXQAA4");
        clientManagement.setAccount(account);
        clientManagement.setRecordTypeId("012O00000009r3eIAA");
        clientManagement.setActivityTime("");
        clientManagement.setListingId("a0EO000000IpLk7MAF");
        clientManagement.setAgentEmail("ngi@independent.com.au");


        clientManagements.add(clientManagement);
        ClientManagement clientManagement2 = new ClientManagement();
        Account account2 = new Account();
        account2.setFirstname("Bill");
        account2.setLastname("Gates");
        account2.setEmail("bill@test.com");
        account2.setLandline("0234245544");
        account2.setMobile("0497574440");
        account2.setRecordTypeId("01228000000LLXQAA4");
        clientManagement2.setAccount(account2);
        clientManagement.setRecordTypeId("01228000000LLXJAA4");

        clientManagements.add(clientManagement2);
        salesforceService.pushDataToSalesforce(optionalPartnerConnection.get(), clientManagements, "" );
    }


    @Test
    public void test11() {
        Optional<PartnerConnection> optionalPartnerConnection = salesforceService.establishConnection(orgId);
        Account account = new Account();
        account.setFirstname("Steve");
        account.setLastname("Jobs");
        account.setEmail("jobs@test.com");
        account.setLandline("0243561792");
        account.setMobile("0409272182");
        Account account2 = new Account();
        account2.setFirstname("Bill");
        account2.setLastname("Gates");
        account2.setEmail("bill@test.com");
        account2.setLandline("0234245544");
        account2.setMobile("0497574440");
        List<Account> accounts = new ArrayList<>();
        accounts.add(account);
        accounts.add(account2);
        salesforceService.findExistingAccounts(optionalPartnerConnection.get(), "", accounts);



    }

    @Test
    public void test22() {
        Optional<PartnerConnection> optionalPartnerConnection = salesforceService.establishConnection("00D2v0000012cBH");

        Assert.assertTrue(optionalPartnerConnection.isPresent());
        List<Activity> clientManagementActivities = new ArrayList<>();
        Activity activity = new Activity();
        activity.setPropAddress("31 Northview Place Mount Colah NSW 2079");
        clientManagementActivities.add(activity);

        Map<String, ListingIdOwnerId> stringListingIdOwnerIdMap = salesforceService.getAddressListingIdOwnerIdMap(optionalPartnerConnection.get(),"prpt__", clientManagementActivities);
        System.out.println();
        List<String> listingIds = new ArrayList<>();
        stringListingIdOwnerIdMap.values().forEach(listingIdOwnerId -> {
            listingIds.add(listingIdOwnerId.getListingId());
        });

        salesforceService.getListingIdOwnerIdMap(optionalPartnerConnection.get(), "prpt__", listingIds);

    }


    @Test
    public void testAddressListingIdOwnerIdMap() {
        Optional<PartnerConnection> optionalPartnerConnection = salesforceService.establishConnection("00D2v0000012cBH");

        Assert.assertTrue(optionalPartnerConnection.isPresent());
        List<Activity> activities = new ArrayList<>();
        Activity activity = new Activity();
        activity.setPropAddress("3 ROE STREET GRIFFITH ACT 2603");
        activities.add(activity);
        salesforceService.getAddressListingIdOwnerIdMap(optionalPartnerConnection.get(), "", activities);

    }

    @Test
    public void test444(){
        Optional<PartnerConnection> optionalPartnerConnection = salesforceService.establishConnection(orgId);

        Map<RecordTypeAndSobjectType, String>  recordTypeAndSobjectTypeIdMap = salesforceService.getRecordTypeSobjectTypeAndIdMap(optionalPartnerConnection.get());
        System.out.println();


    }
}
