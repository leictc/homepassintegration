package com.propic.homepassintegration;

import org.springframework.beans.factory.annotation.Value;

public class HomepassIntegrationApplicationTests {


	@Value("${sqs.message_queue_url}")
	private String messageQueueUrl;

	@Value("${sqs.access_key}")
	private String accessKey;

	@Value("${sqs.access_token}")
	private String accessToken;

	@Value("${sqs.region}")
	private String region;

	/*
	@Test
	public void contextLoads() {
	}*/



}
