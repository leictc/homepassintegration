package com.propic.homepassintegration;


import com.propic.homepassintegration.entity.HomepassIntegrationRequest;
import com.propic.homepassintegration.entity.IntegrationOffice;
import com.propic.homepassintegration.entity.OfficeSynchroniseDatetime;
import com.propic.homepassintegration.entity.OfficeTimeId;
import com.propic.homepassintegration.service.DBService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationContextLoader;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes=HomepassIntegrationApplication.class, loader= SpringApplicationContextLoader.class)
public class DBServiceTest {

    private static final String ORG_ID ="00DO00000055v14MAA";
    private static final String NAMESPACE ="prpt_";
    private static final String OFFICE_ID = "5630be58204b89c90b2034ea";

    @Autowired
    private DBService service;

    @Test
    public void test(){
        HomepassIntegrationRequest homepassIntegrationRequest = new HomepassIntegrationRequest();
        homepassIntegrationRequest.setOrgId(ORG_ID);
        homepassIntegrationRequest.setNamespace(NAMESPACE);

        IntegrationOffice integrationOffice = new IntegrationOffice();
        integrationOffice.setOfficeId(OFFICE_ID);
        integrationOffice.setRequest(homepassIntegrationRequest);

        List<IntegrationOffice> integrationOffices = new ArrayList<>();
        integrationOffices.add(integrationOffice);

        homepassIntegrationRequest.setIntegrationOffices(integrationOffices);
        service.saveRequest(homepassIntegrationRequest);
        Optional<HomepassIntegrationRequest> integrationRequestOptional = service.getRequest(ORG_ID);
        assertTrue(integrationRequestOptional.isPresent());
        HomepassIntegrationRequest homepassIntegrationRequestFromDB = integrationRequestOptional.get();
        assertEquals(homepassIntegrationRequest.getOrgId(), homepassIntegrationRequestFromDB.getOrgId());
        assertEquals(homepassIntegrationRequest.getNamespace(), homepassIntegrationRequestFromDB.getNamespace());
        List<IntegrationOffice> integrationOfficesFromDB = homepassIntegrationRequestFromDB.getIntegrationOffices();
        assertEquals(integrationOffices.size(), integrationOfficesFromDB.size());
        assertEquals(integrationOffices.get(0).getOfficeId(), integrationOfficesFromDB.get(0).getOfficeId());

        Optional<IntegrationOffice> optionalIntegrationOffice = service.getOffice(OFFICE_ID);
        assertTrue(optionalIntegrationOffice.isPresent());

        service.deleteRequest(ORG_ID);
        //ensure everything deleted
        integrationRequestOptional = service.getRequest(ORG_ID);
        assertFalse(integrationRequestOptional.isPresent());
        optionalIntegrationOffice = service.getOffice(OFFICE_ID);
        assertFalse(optionalIntegrationOffice.isPresent());


    }

    @Test
    public void testOfficeSynchroniseDatetime(){
        final String DATETIME = "2019-01-16T23:17:45.053Z";
        service.deleteAllOfficeSynchroniseDatetimes();
        OfficeTimeId officeTimeId = new OfficeTimeId(ORG_ID, OFFICE_ID);
        OfficeSynchroniseDatetime officeSynchroniseDatetime = new OfficeSynchroniseDatetime(officeTimeId, DATETIME);

        Optional<OfficeSynchroniseDatetime> optionalOfficeSynchroniseDatetime = service.getOfficeSynchroniseDatetime(officeTimeId);
        assertFalse(optionalOfficeSynchroniseDatetime.isPresent());

        service.saveOfficeSynchroniseDatetime(officeSynchroniseDatetime);
        optionalOfficeSynchroniseDatetime = service.getOfficeSynchroniseDatetime(officeTimeId);
        assertTrue(optionalOfficeSynchroniseDatetime.isPresent());
        assertEquals(ORG_ID, optionalOfficeSynchroniseDatetime.get().getId().getOrgId());
        assertEquals(OFFICE_ID, optionalOfficeSynchroniseDatetime.get().getId().getOfficeId());
        assertEquals(DATETIME, optionalOfficeSynchroniseDatetime.get().getLastSynchroniseDatetime());


    }
}
