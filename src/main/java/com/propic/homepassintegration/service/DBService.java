package com.propic.homepassintegration.service;

import com.propic.homepassintegration.entity.HomepassIntegrationRequest;
import com.propic.homepassintegration.entity.IntegrationOffice;
import com.propic.homepassintegration.entity.OfficeSynchroniseDatetime;
import com.propic.homepassintegration.entity.OfficeTimeId;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface DBService {
    void saveRequest(HomepassIntegrationRequest request);

    Optional<HomepassIntegrationRequest> getRequest(String orgId);

    List<HomepassIntegrationRequest> findAllRequests();

    void deleteAllRequests();

    void deleteRequest(String orgId);

    Optional<IntegrationOffice> getOffice(String officeId);

    Optional<OfficeSynchroniseDatetime> getOfficeSynchroniseDatetime(OfficeTimeId officeTimeId);

    void saveOfficeSynchroniseDatetime(OfficeSynchroniseDatetime officeSynchroniseDatetime);

    void deleteAllOfficeSynchroniseDatetimes();

    List<OfficeSynchroniseDatetime> findAllOfficeSynchroniseDatetimes();
}
