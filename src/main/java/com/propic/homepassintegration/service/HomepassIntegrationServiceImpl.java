package com.propic.homepassintegration.service;

import com.propic.homepassintegration.entity.HomepassIntegrationRequest;
import com.propic.homepassintegration.entity.OfficeTimeId;
import com.propic.homepassintegration.entity.OrganisationHomepassData;
import com.propic.homepassintegration.response.HomepassData;
import com.propic.homepassintegration.util.HomepassUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class HomepassIntegrationServiceImpl implements HomepassIntegrationService {
    private static final Logger LOG = LogManager.getLogger(HomepassIntegrationServiceImpl.class.getName());


    @Autowired
    private HomepassAPISecretService homepassAPISecretService;

    @Autowired
    private DBService dbService;

    @Autowired
    private HomepassAPIService homepassAPIService;

    @Autowired
    private SalesforceService salesforceService;

    @Override
    public void processRequests() {
        LOG.info("-------------------HomepassIntegrationScheduler is running---------------------");

        Map<String, String> orgSecretMap= constructOrgSecretMap();
        if (orgSecretMap.isEmpty()){
            LOG.info("Fail to retrieve homepass api secret information from DB, terminate the processing");
            return;
        }
        Map<String, String> officeDatetimeMap= constructOfficeDatetimeMapping();
        if (officeDatetimeMap.isEmpty()){
            LOG.info("Fail to retrieve synchronisation datetime information from DB, terminate the processing");
            return;
        }

        List<OrganisationHomepassData> organisationHomepassDataList = retrieveHomepassData(dbService.findAllRequests(), orgSecretMap, officeDatetimeMap);
        LOG.info("--------------------------------------");

    }

    private Map<String, String> constructOrgSecretMap(){
        Map<String, String> orgSecretMap= new HashMap<>();
        homepassAPISecretService.findAllAPISecrets().forEach(homepassAPISecret -> {
            orgSecretMap.put(homepassAPISecret.getOrgId(), homepassAPISecret.getSecret());
        });
        return orgSecretMap;

    }

    public Map<String, String> constructOfficeDatetimeMapping(){
        Map<String, String> map = new HashMap<>();
        dbService.findAllOfficeSynchroniseDatetimes().forEach(officeSynchroniseDatetime -> {
            OfficeTimeId officeTimeId = officeSynchroniseDatetime.getId();
            map.put( officeTimeId.getOfficeId(), officeSynchroniseDatetime.getLastSynchroniseDatetime());
        });
        return map;
    }

    private List<OrganisationHomepassData> retrieveHomepassData(final List<HomepassIntegrationRequest> homepassIntegrationRequests,
                                                                final Map<String, String> orgSecretMap, final Map<String, String> officeDatetimeMap){
        final List<OrganisationHomepassData> organisationHomepassDataList = new ArrayList<>();
        homepassIntegrationRequests.forEach(request -> {
            LOG.info(request);
            final String secret = orgSecretMap.get(request.getOrgId());
            List<HomepassData> homepassDataList = new ArrayList<>();
            request.getIntegrationOffices().forEach(integrationOffice -> {
                final String officeId =integrationOffice.getOfficeId();
                final String sinceDatetime =officeDatetimeMap.get(officeId);
                homepassAPIService.retrieveHomepassData(officeId, secret, sinceDatetime).ifPresent(homepassData -> {
                    dbService.getOfficeSynchroniseDatetime(new OfficeTimeId(request.getOrgId(), officeId))
                            .ifPresent(officeSynchroniseDatetime -> {
                                officeSynchroniseDatetime.setLastSynchroniseDatetime(HomepassUtil.toCurrentUtcDatetimeString());
                                dbService.saveOfficeSynchroniseDatetime(officeSynchroniseDatetime);
                            });
                    homepassDataList.add(homepassData);

                });

            });
            organisationHomepassDataList.add(new OrganisationHomepassData(request.getOrgId(), homepassDataList));
        });
        return organisationHomepassDataList;
    }

    private void pushToSalesforce(final List<OrganisationHomepassData> organisationHomepassDataList){

        /*organisationHomepassDataList.forEach(organisationHomepassData -> {
            salesforceService.prepareDataForSalesforce(organisationHomepassData);

        });*/


    }
}
