package com.propic.homepassintegration.service;

public interface HomepassIntegrationService {
    void processRequests();
}
