package com.propic.homepassintegration.service;

import com.propic.homepassintegration.entity.*;
import com.sforce.soap.partner.PartnerConnection;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface SalesforceService {
    void pushDataToSalesforce(PartnerConnection connection, List<ClientManagement> clientManagements, String namespace);

    void findExistingAccounts(PartnerConnection connection, String namespace, List<Account> accounts);

    Map<String,String> getListingIdOwnerIdMap(PartnerConnection connection, String namespace, List<String> listingIds);

    Map<String, ListingIdOwnerId> getAddressListingIdOwnerIdMap(PartnerConnection connection, String namespace, List<Activity> clientManagementActivities);

    Optional<PartnerConnection> establishConnection(String orgId);

    Map<RecordTypeAndSobjectType, String>  getRecordTypeSobjectTypeAndIdMap(PartnerConnection conn);

    //Map<String, List<Activity>> prepareDataForSalesforce(List<OrganisationHomepassData> organisationHomepassDataList);
}
