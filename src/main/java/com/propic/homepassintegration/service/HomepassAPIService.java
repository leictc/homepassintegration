package com.propic.homepassintegration.service;

import com.propic.homepassintegration.response.HomepassData;

import java.util.Optional;

public interface HomepassAPIService {

    Optional<HomepassData> retrieveHomepassData(String officeId, String secret, String sinceDatetime);
}
