package com.propic.homepassintegration.service;

import com.ctc.salesforce.enhanced.adaptor.SalesforceAgent;
import com.propic.homepassintegration.entity.*;
import com.propic.homepassintegration.util.SalesforceServiceUtil;
import com.sforce.soap.partner.PartnerConnection;
import com.sforce.soap.partner.QueryResult;
import com.sforce.soap.partner.SaveResult;
import com.sforce.soap.partner.sobject.SObject;
import com.sforce.ws.ConnectionException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class SalesforceServiceImpl implements SalesforceService {
    private static final Logger LOG = LogManager.getLogger(SalesforceServiceImpl.class.getName());



    public List<ClientManagement> getClientManagement(final PartnerConnection connection, final List<Activity> activities, final Map<RecordTypeAndSobjectType, String> recordTypeAndSobjectTypeIdMap,  final String namespace){
        LOG.info("Preparing client managements");
        final List<ClientManagement> clientManagements = new ArrayList<>();

        if (recordTypeAndSobjectTypeIdMap.isEmpty()){
            LOG.info("No record type id information , no information gonna push to salesforce");
            return clientManagements;
        }
        final List<Activity> clientManagementActivities = activities.stream().filter(activity -> activity.getType().equals(ClientManagementType.RESIDENTIAL)).collect(Collectors.toList());

        if(!clientManagementActivities.isEmpty()) {
            final Map<String, ListingIdOwnerId> addressListingIdOwnerIdMap = getAddressListingIdOwnerIdMap(connection, namespace, clientManagementActivities);
            if (!addressListingIdOwnerIdMap.isEmpty()) {
                List<String> listingIds = new ArrayList<>();
                addressListingIdOwnerIdMap.values().forEach(listingIdOwnerId -> {
                    listingIds.add(listingIdOwnerId.getListingId());
                });

                Map<String,String> listingIdOwnerIdMap = getListingIdOwnerIdMap(connection, "prpt__", listingIds);

                activities.forEach(activity -> {
                    SalesforceServiceUtil.toClientManagement(activity, recordTypeAndSobjectTypeIdMap, addressListingIdOwnerIdMap, listingIdOwnerIdMap).ifPresent(clientManagements::add);
                });
            }
            else{
                LOG.info("No listing id and owner id match up any addresses");
            }
        }
        //do something
        return clientManagements;

    }

    private void createAccounts(final PartnerConnection connection, final List<SObject> accountSobjs, final List<Account> accounts){
        try {
            SaveResult[] results = connection.create(accountSobjs.toArray(new SObject[] {}));
            int i=0;
            for( SaveResult saveResult: results) {
                if (saveResult.isSuccess()) {
                    LOG.info("Account " + saveResult.getId() +" "+accounts.get(i)+ " is created successfully");
                    accounts.get(i).setId(saveResult.getId());
                    accounts.get(i).setExistence(true);
                } else {
                    com.sforce.soap.partner.Error[] errors = saveResult.getErrors();
                    for (com.sforce.soap.partner.Error error : errors) {
                        LOG.error("Fail to create account "+accounts.get(i)+" " + saveResult.getId() + " , " + Arrays.toString(error.getFields()) + " , " + error.getMessage());
                    }
                }
                i++;
            }
        } catch (ConnectionException e) {
            LOG.error("Fail to create accounts on salesforce due to "+e);
        }
    }

    private void createClientCards(final PartnerConnection connection, final List<SObject> clientCardSobjs, final List<Account> accounts) {
        try {
            SaveResult[] results = connection.create(clientCardSobjs.toArray(new SObject[]{}));
            int i = 0;
            for (SaveResult saveResult : results) {
                if (saveResult.isSuccess()) {
                    LOG.info("Client Card " + saveResult.getId() + " for " + accounts.get(i) + " is created successfully");
                } else {
                    com.sforce.soap.partner.Error[] errors = saveResult.getErrors();
                    for (com.sforce.soap.partner.Error error : errors) {
                        LOG.error("Fail to create account for " + accounts.get(i) + " " + saveResult.getId() + " , " + Arrays.toString(error.getFields()) + " , " + error.getMessage());
                    }
                }
                i++;
            }
        } catch (ConnectionException e) {
            LOG.error("Fail to create accounts on salesforce due to " + e);
        }
    }

    private void createInspections(final PartnerConnection connection, final List<SObject> inspectionAttendeeSobjs, final List<InspectionAttendee> inspectionAttendees) {
        try {
            SaveResult[] results = connection.create(inspectionAttendeeSobjs.toArray(new SObject[] {}));
            int i=0;
            for( SaveResult saveResult: results) {
                if (saveResult.isSuccess()) {
                    LOG.info("Inspection Attendee " + saveResult.getId() +" "+inspectionAttendees.get(i)+ " is created successfully");
                    inspectionAttendees.get(i).setId(saveResult.getId());
                } else {
                    com.sforce.soap.partner.Error[] errors = saveResult.getErrors();
                    for (com.sforce.soap.partner.Error error : errors) {
                        LOG.error("Fail to create inspection attendee "+inspectionAttendees.get(i)+" " + saveResult.getId() + " , " + Arrays.toString(error.getFields()) + " , " + error.getMessage());
                    }
                }
                i++;
            }
        } catch (ConnectionException e) {
            LOG.error("Fail to create inspection attendees on salesforce due to "+e);
        }
    }

    private void createClientManagement(final PartnerConnection connection, final List<SObject> clientManagementSobjs, final  List<ClientManagement> clientManagements) {
        try {
            SaveResult[] results = connection.create(clientManagementSobjs.toArray(new SObject[] {}));
            int i=0;
            for( SaveResult saveResult: results) {
                if (saveResult.isSuccess()) {
                    LOG.info("Client management " + saveResult.getId() +" "+clientManagements.get(i)+ " is created successfully");
                } else {
                    com.sforce.soap.partner.Error[] errors = saveResult.getErrors();
                    for (com.sforce.soap.partner.Error error : errors) {
                        LOG.error("Fail to create client management "+clientManagements.get(i)+" " + saveResult.getId() + " , " + Arrays.toString(error.getFields()) + " , " + error.getMessage());
                    }
                }
                i++;
            }
        } catch (ConnectionException e) {
            LOG.error("Fail to create client managements on salesforce due to "+e);
        }
    }

    @Override
    public void pushDataToSalesforce(final PartnerConnection connection, final List<ClientManagement> clientManagements, final String namespace) {
        List<Account> accounts = new ArrayList<>();
        List<InspectionAttendee> inspectionAttendees = new ArrayList<>();

        List<SObject> accountSobjs = new ArrayList<>();
        List<SObject> inspectionAttendeeSobjs = new ArrayList<>();
        List<SObject> clientManagementSobjs = new ArrayList<>();
        List<SObject> clientCardSobjs = new ArrayList<>();

        clientManagements.forEach(clientManagement -> {
            accounts.add(clientManagement.getAccount());
            inspectionAttendees.add(clientManagement.getInspectionAttendee());
        });
        findExistingAccounts(connection, namespace, accounts);

        //Only create new accounts
        List<Account> newAccounts = accounts.stream().filter(Account::isExistence).collect(Collectors.toList());
        newAccounts.forEach(account -> {
            accountSobjs.add(account.toSObject());
        });

        inspectionAttendees.forEach(inspectionAttendee -> inspectionAttendeeSobjs.add(inspectionAttendee.toSObject(namespace)));
        createAccounts(connection, accountSobjs, newAccounts);

        List<Account> clientCardAccounts = new ArrayList<>();
        newAccounts.forEach(account -> {
            if (account.isExistence()) {
                clientCardSobjs.add(account.toClientCardSObject(namespace));
                clientCardAccounts.add(account);
            }
        });

        //create client cards
        createClientCards(connection, clientCardSobjs, clientCardAccounts);

        // create inspections
        createInspections(connection, inspectionAttendeeSobjs, inspectionAttendees);
        clientManagements.forEach(clientManagement -> {
            clientManagementSobjs.add(clientManagement.toBuyerManagementSObject(namespace));
        });

        //create client managements
        createClientManagement(connection, clientManagementSobjs, clientManagements);
    }

    @Override
    public void findExistingAccounts(final PartnerConnection connection, final String namespace, final List<Account> accounts) {
        List<Account> existingAccounts = new ArrayList<>();
        try {
            QueryResult queryResult = connection.query(SalesforceServiceUtil.createAccountQuery(accounts, namespace));
            boolean done = false;
            if (queryResult.getSize() > 0) {
                while (!done) {
                    for (SObject sobj : queryResult.getRecords()) {
                        Account existingAccount  = new Account();
                        existingAccount.setId((String) sobj.getField("Id"));
                        existingAccount.setEmail((String) sobj.getField("PersonEmail"));
                        existingAccount.setMobile((String) sobj.getField("PersonMobilePhone"));
                        existingAccount.setLandline((String) sobj.getField("PersonHomePhone"));
                        existingAccount.setLastname((String) sobj.getField("LastName"));
                        existingAccount.setFirstname((String) sobj.getField("FirstName"));
                        existingAccounts.add(existingAccount);
                    }
                    if (queryResult.isDone()) {
                        done = true;
                    } else {
                        queryResult = connection.queryMore(queryResult.getQueryLocator());
                    }
                }
            }
        } catch (ConnectionException e) {
            LOG.error("Fail to query the accounts due to "+e);
        }

        accounts.forEach(account -> {
            existingAccounts.forEach(exisitngAccount ->{
                if (account.match(exisitngAccount)){
                    account.setExistence(true);
                    account.setId(exisitngAccount.getId());
                }
            });
        });
    }


    @Override
    public Map<String,String> getListingIdOwnerIdMap(final PartnerConnection connection, final String namespace, final List<String> listingIds){
        Map<String,String> listingIdOwnerIdMap = new HashMap<>();
        String listingIdConditions = " where " + namespace+"Listing__c"+" IN (";
        int idx = 0;

        for(String id  : listingIds){
            if(idx==0){
                listingIdConditions = listingIdConditions+"'"+id +"'";
            }else{
                listingIdConditions = listingIdConditions+", '"+id +"'";
            }
            idx++;
        }

        if(idx == 0 ){
            return listingIdOwnerIdMap;
        }

        listingIdConditions = listingIdConditions+")";

        String queryString = "select "
                + "id, "
                + namespace+"Listing__c, "
                + namespace+"User_Extension_ID__r."+namespace+"Related_User__c "
                + "from " + namespace+"Commission_Split__c "
                +  listingIdConditions;

        try {
            QueryResult queryResult = connection.query(queryString);
            boolean done = false;
            if (queryResult.getSize() > 0) {
                while (!done) {
                    for (SObject sobj : queryResult.getRecords()) {
                        String listingId = (String) sobj.getField(namespace+"Listing__c");
                        String coAgentId = (String) sobj.getChild(namespace+"User_Extension_ID__r").getField(namespace+"Related_User__c");
                        listingIdOwnerIdMap.put(listingId, coAgentId);

                    }
                    if (queryResult.isDone()) {
                        done = true;
                    } else {
                        queryResult = connection.queryMore(queryResult.getQueryLocator());
                    }
                }
            }
        } catch (ConnectionException e) {
            LOG.error("Fail to retrieve listing Id and owner Id from Commission_Split__c table due to "+e);
        }
        return listingIdOwnerIdMap;
    }

    @Override
    public Map<String,ListingIdOwnerId> getAddressListingIdOwnerIdMap(final PartnerConnection connection, final String namespace, final List<Activity> clientManagementActivities){
        final Map<String,ListingIdOwnerId> addressListingIdOwnerIdMap = new HashMap<>();
        try {
            QueryResult queryResult = connection.query(SalesforceServiceUtil.createAddressListingIdOwnerIdMapQuery(clientManagementActivities, namespace));

            boolean done = false;
            if (queryResult.getSize() > 0) {
                while (!done) {
                    for (SObject sobj : queryResult.getRecords()) {
                        //get listing id and owner id
                        String listingId = (String) sobj.getField("Id");
                        String ownerId = (String) sobj.getField("OwnerId");
                        String address = (String) sobj.getField(namespace+"Property_Address__c");
                        ListingIdOwnerId listingIdOwnerId = new ListingIdOwnerId(listingId, ownerId);
                        if (listingIdOwnerId.isValid())
                            addressListingIdOwnerIdMap.put(address, listingIdOwnerId);
                    }
                    if (queryResult.isDone()) {
                        done = true;
                    } else {
                        queryResult = connection.queryMore(queryResult.getQueryLocator());
                    }
                }
            }
        } catch (ConnectionException e) {
            LOG.error("Fail to retrieve listing id and owner id from Appraisal_Listing__c table due to "+e);

        }
        return addressListingIdOwnerIdMap;

    }

    @Override
    public Optional<PartnerConnection> establishConnection(final String orgId){
        LOG.info("Connecting salesforce organisation "+orgId);

        PartnerConnection connection;
        try {
            connection = SalesforceAgent.getPropertyConnectionByOAuth(orgId);
        } catch (ConnectionException e) {
            LOG.error("Fail to establish connection to organisation:"+orgId+" due to :"+e);
            return Optional.empty();
        }
        return Optional.ofNullable(connection);

    }

    @Override
    public Map<RecordTypeAndSobjectType, String>  getRecordTypeSobjectTypeAndIdMap(final PartnerConnection conn)  {
        Map<RecordTypeAndSobjectType, String> recordTypeSobjectTypeAndIdMap = new HashMap<>();

        try {
            LOG.info("Retrieving record type id information " );
            //query the record type id from salesforce instance
            String queryString = "Select Id, DeveloperName, SobjectType From RecordType where DeveloperName  in ('Inspection_Attendee', 'Enquiry', 'Individual_Clients' ) and SobjectType in ('Buyer_Management_1__c', 'Client_Management__c','Account' ) ";
            QueryResult queryResult = conn.query(queryString);
            for (SObject sobj : queryResult.getRecords()) {
                String id = (String) sobj.getField("Id");
                String recordType = (String) sobj.getField("DeveloperName");
                String sobjectType = (String) sobj.getField("SobjectType");
                recordTypeSobjectTypeAndIdMap.put(new RecordTypeAndSobjectType(recordType, sobjectType), id);
            }
        } catch (Exception e) {
            LOG.error("Fail to retrieve record type id information ");

        }
        return recordTypeSobjectTypeAndIdMap;
    }


}
