package com.propic.homepassintegration.service;

import com.amazonaws.services.sqs.model.Message;

import java.util.List;

/*
{"projectOfficeIdList":["5cac3f4dc1c2c51ba347e001"],"orgId":"00DO00000055v14MAA",
"officeIdList":["5630be58204b89c90b2034ea"],"namespace":"prpt__","homepassAPISecret":"sv4dMBArt3eaVhqe87QVfqQez3xTVncNy"}

 */
public interface MessageQueueService {
    void sendMessage(String messageBody);

    List<Message> retrieveMessage();

    void deleteMessage(Message message);
}
