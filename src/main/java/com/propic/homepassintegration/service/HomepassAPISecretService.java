package com.propic.homepassintegration.service;

import com.propic.homepassintegration.entity.HomepassAPISecret;

import java.util.List;
import java.util.Optional;

public interface HomepassAPISecretService {
    Optional<HomepassAPISecret> retrieveAPISecret(String orgId);

    void saveAPISecret(String orgId, String secret);

    List<HomepassAPISecret> findAllAPISecrets();
}
