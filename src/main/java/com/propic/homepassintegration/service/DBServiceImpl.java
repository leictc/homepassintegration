package com.propic.homepassintegration.service;

import com.propic.homepassintegration.entity.HomepassIntegrationRequest;
import com.propic.homepassintegration.entity.IntegrationOffice;
import com.propic.homepassintegration.entity.OfficeSynchroniseDatetime;
import com.propic.homepassintegration.entity.OfficeTimeId;
import com.propic.homepassintegration.repository.HomepassIntegrationRequestRepository;
import com.propic.homepassintegration.repository.IntegrationOfficeRepository;
import com.propic.homepassintegration.repository.OfficeSynchroniseDatetimeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DBServiceImpl implements DBService {

    @Autowired
    private HomepassIntegrationRequestRepository homepassIntegrationRequestRepository;

    @Autowired
    private IntegrationOfficeRepository integrationOfficeRepository;


    @Autowired
    private OfficeSynchroniseDatetimeRepository officeSynchroniseDatetimeRepository;

    @Override
    public void saveRequest(HomepassIntegrationRequest request) {
        homepassIntegrationRequestRepository.save(request);
    }

    @Override
    public Optional<HomepassIntegrationRequest> getRequest(String orgId) {
        return Optional.ofNullable(homepassIntegrationRequestRepository.findOne(orgId));
    }

    @Override
    public List<HomepassIntegrationRequest> findAllRequests(){
        return homepassIntegrationRequestRepository.findAll();
    }

    @Override
    public void deleteAllRequests(){
        homepassIntegrationRequestRepository.deleteAll();
    }

    @Override
    public void deleteRequest(String orgId) {
        homepassIntegrationRequestRepository.delete(orgId);
    }

    @Override
    public Optional<IntegrationOffice> getOffice(String officeId) {
        return Optional.ofNullable(integrationOfficeRepository.findOne(officeId));
    }

    @Override
    public Optional<OfficeSynchroniseDatetime> getOfficeSynchroniseDatetime(OfficeTimeId officeTimeId){
        return Optional.ofNullable(officeSynchroniseDatetimeRepository.findOne(officeTimeId));
    }

    @Override
    public void saveOfficeSynchroniseDatetime(OfficeSynchroniseDatetime officeSynchroniseDatetime){
        officeSynchroniseDatetimeRepository.save(officeSynchroniseDatetime);
    }

    @Override
    public void deleteAllOfficeSynchroniseDatetimes(){
        officeSynchroniseDatetimeRepository.deleteAll();
    }

    @Override
    public List<OfficeSynchroniseDatetime> findAllOfficeSynchroniseDatetimes(){
        return officeSynchroniseDatetimeRepository.findAll();
    }


}
