package com.propic.homepassintegration.service;

import com.propic.homepassintegration.entity.HomepassAPISecret;
import com.propic.homepassintegration.repository.HomepassAPISecretRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class HomepassAPISecretServiceImpl implements HomepassAPISecretService {
    private static final Logger LOG = LogManager.getLogger(HomepassAPISecretServiceImpl.class.getName());



    @Autowired
    private HomepassAPISecretRepository homepassAPISecretRepository;

    @Override
    public Optional<HomepassAPISecret> retrieveAPISecret(final String orgId){
        return Optional.ofNullable(homepassAPISecretRepository.findOne(orgId));
        //return Optional.empty();
    }

    @Override
    public void saveAPISecret(final String orgId, final String secret){
        HomepassAPISecret homepassAPISecret = new HomepassAPISecret();
        homepassAPISecret.setOrgId(orgId);
        homepassAPISecret.setSecret(secret);
        homepassAPISecretRepository.save(homepassAPISecret);
    }

    @Override
    public List<HomepassAPISecret> findAllAPISecrets(){
        return homepassAPISecretRepository.findAll();
    }

}
