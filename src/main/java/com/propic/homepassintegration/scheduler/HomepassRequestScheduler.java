package com.propic.homepassintegration.scheduler;

import com.propic.homepassintegration.entity.HomepassIntegrationRequest;
import com.propic.homepassintegration.entity.OfficeSynchroniseDatetime;
import com.propic.homepassintegration.entity.OfficeTimeId;
import com.propic.homepassintegration.service.DBService;
import com.propic.homepassintegration.service.HomepassAPISecretService;
import com.propic.homepassintegration.service.MessageQueueService;
import com.propic.homepassintegration.util.HomepassUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class HomepassRequestScheduler {
    private static final Logger LOG = LogManager.getLogger(HomepassRequestScheduler.class.getName());

    @Autowired
    private MessageQueueService messageQueueService;

    @Autowired
    private HomepassAPISecretService homepassAPISecretService;


    @Autowired
    private DBService dbService;

    @Scheduled(cron = "0/20 * * * * *")
    public void processRequests() {
        LOG.info("Processing homepass integration requests");
        saveRequestInformation(retrieveReuqest());
        LOG.info("***********************************************");
    }

    private List<HomepassIntegrationRequest> retrieveReuqest(){
        final List<HomepassIntegrationRequest> homepassIntegrationRequests = new ArrayList<>();
        messageQueueService.retrieveMessage().forEach(message -> {
            HomepassUtil.toHomepassIntegrationRequest(message.getBody()).map(homepassIntegrationRequests::add);
            messageQueueService.deleteMessage(message);
        });

        int numberOfRequestReceived = homepassIntegrationRequests.size();
        if (numberOfRequestReceived >0)
            LOG.info(numberOfRequestReceived+" Homepass Integration Requests received successfully");
        else
            LOG.info("No Homepass Integration Request received");
        return homepassIntegrationRequests;
    }

    private void saveRequestInformation(final List<HomepassIntegrationRequest> homepassIntegrationRequests){
        LOG.info("Attempting to save request information in database");
        if (!homepassIntegrationRequests.isEmpty())
            dbService.deleteAllRequests();
        homepassIntegrationRequests.forEach(request -> {
            LOG.info(request);
            dbService.saveRequest(request);
            homepassAPISecretService.saveAPISecret(request.getOrgId(), request.getSecret());
            request.getIntegrationOffices().forEach(integrationOffice -> {
                OfficeTimeId officeTimeId = new OfficeTimeId(request.getOrgId(), integrationOffice.getOfficeId());
                Optional<OfficeSynchroniseDatetime> optionalOfficeSynchroniseDatetime = dbService.getOfficeSynchroniseDatetime(officeTimeId);
                if (!optionalOfficeSynchroniseDatetime.isPresent()){
                    dbService.saveOfficeSynchroniseDatetime(new OfficeSynchroniseDatetime(officeTimeId, HomepassUtil.toCurrentUtcDatetimeString()));
                }
            });

        });
    }

}
