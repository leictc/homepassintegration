package com.propic.homepassintegration.scheduler;

import com.propic.homepassintegration.entity.*;
import com.propic.homepassintegration.response.HomepassData;
import com.propic.homepassintegration.service.*;
import com.propic.homepassintegration.util.HomepassUtil;
import com.propic.homepassintegration.util.SalesforceServiceUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class HomepassIntegrationScheduler {
    private static final Logger LOG = LogManager.getLogger(HomepassIntegrationScheduler.class.getName());

    @Autowired
    private HomepassAPISecretService homepassAPISecretService;

    @Autowired
    private DBService dbService;

    @Autowired
    private HomepassAPIService homepassAPIService;

    @Autowired
    private SalesforceService salesforceService;


    @Scheduled(cron = "${scheduler.cron}")
    public void run() {
        LOG.info("\n-------------------HomepassIntegrationScheduler is running---------------------");
        List<OrganisationHomepassData> organisationHomepassDataList = retrieveHomepassData(dbService.findAllRequests(), constructOrgSecretMap(), constructOfficeDatetimeMapping());


        LOG.info("---------------------------------------------------------------------------------\n");

    }


    private Map<String, String> constructOrgSecretMap(){
        Map<String, String> orgSecretMap= new HashMap<>();
        homepassAPISecretService.findAllAPISecrets().forEach(homepassAPISecret -> {
            orgSecretMap.put(homepassAPISecret.getOrgId(), homepassAPISecret.getSecret());
        });
        return orgSecretMap;

    }

    private Map<String, String> constructOfficeDatetimeMapping(){
        Map<String, String> map = new HashMap<>();
        dbService.findAllOfficeSynchroniseDatetimes().forEach(officeSynchroniseDatetime -> {
            OfficeTimeId officeTimeId = officeSynchroniseDatetime.getId();
            map.put( officeTimeId.getOfficeId(), officeSynchroniseDatetime.getLastSynchroniseDatetime());
        });
        return map;
    }

    public List<OrganisationHomepassData> retrieveHomepassData(final List<HomepassIntegrationRequest> homepassIntegrationRequests,
                                                                final Map<String, String> orgSecretMap, final Map<String, String> officeDatetimeMap){
        final List<OrganisationHomepassData> organisationHomepassDataList = new ArrayList<>();

        if (orgSecretMap.isEmpty()){
            LOG.info("Fail to retrieve homepass api secret information from DB, terminate the processing");
            return organisationHomepassDataList;

        }
        if (officeDatetimeMap.isEmpty()){
            LOG.info("Fail to retrieve synchronisation datetime information from DB, terminate the processing");
            return organisationHomepassDataList;

        }
        homepassIntegrationRequests.forEach(request -> {
            LOG.info(request);
            final String orgId = request.getOrgId();
            final String secret = orgSecretMap.get(orgId);
            LOG.info("Attempting to retrieve data from homepass for organisation "+orgId);
            List<HomepassData> homepassDataList = new ArrayList<>();
            request.getIntegrationOffices().forEach(integrationOffice -> {
                final String officeId =integrationOffice.getOfficeId();
                final String sinceDatetime =officeDatetimeMap.get(officeId);
                //Retrieve data from homepass for each office belong to the organisation
                homepassAPIService.retrieveHomepassData(officeId, secret, sinceDatetime).ifPresent(homepassData -> {
                    homepassData.setOfficeId(officeId);
                    homepassDataList.add(homepassData);
                });
                //Update synchronisation datetime
                dbService.getOfficeSynchroniseDatetime(new OfficeTimeId(orgId, officeId))
                        .ifPresent(officeSynchroniseDatetime -> {
                            officeSynchroniseDatetime.setLastSynchroniseDatetime(HomepassUtil.toCurrentUtcDatetimeString());
                            dbService.saveOfficeSynchroniseDatetime(officeSynchroniseDatetime);
                        });

            });
            organisationHomepassDataList.add(new OrganisationHomepassData(orgId, homepassDataList));
        });
        return organisationHomepassDataList;
    }

    private void pushToSalesforce(final List<OrganisationHomepassData> organisationHomepassDataList){
        Map<String, List<Activity>> organisationActivitiesMap =  SalesforceServiceUtil.parseHomepassData(organisationHomepassDataList);
        /*
        organisationActivitiesMap.forEach((orgId, activities) -> {
            salesforceService.establishConnection(orgId).ifPresent(conn -> {

                Map<RecordTypeAndSobjectType, String>  recordTypeAndSobjectTypeIdMap = salesforceService.getRecordTypeSobjectTypeAndIdMap(conn);
                //salesforceService.


            });

        });*/




    }




}
