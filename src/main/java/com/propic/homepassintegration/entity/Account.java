package com.propic.homepassintegration.entity;

import com.sforce.soap.partner.sobject.SObject;
import org.apache.commons.lang.StringUtils;

public class Account {
    public static final String RECORD_TYPE = "Individual_Clients";
    public static final String SOBJECT = "Account";

    private String id;
    private String firstname;
    private String lastname="";
    private String email="";
    private String mobile="";
    private String landline="";
    private Address address;
    private String recordTypeId;
    private String ownerId;
    private String ownerIdForClientCard;
    private boolean existence = false;



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public SObject toSObject(){
        SObject sobj = new SObject();
        sobj.setType(SOBJECT);

        sobj.setField("FirstName", getFirstname());
        sobj.setField("LastName", getLastname());
        sobj.setField("PersonEmail", getEmail());
        sobj.setField("PersonMobilePhone", getMobile());
        sobj.setField("PersonHomePhone", getLandline());
        sobj.setField("OwnerId",getOwnerId());
        sobj.setField("PersonLeadSource","Inspection");

        sobj.setField("RecordTypeId",getRecordTypeId());
        if(getAddress() != null){
            String street = getAddress().getStreet();
            if(!StringUtils.isEmpty( street ) && !StringUtils.isEmpty( getAddress().getStreetNo())){
                street = getAddress().getStreetNo().trim() + " " + street;
            }
            sobj.setField("BillingStreet",street);
            sobj.setField("BillingState",getAddress().getState());
            sobj.setField("BillingPostalCode",getAddress().getPostcode());
            sobj.setField("BillingCountry",getAddress().getCountry());
            sobj.setField("BillingCity",getAddress().getCity());
        }

        return sobj;
    }

    public SObject toClientCardSObject(final String namespace) {
        SObject sobj = new SObject();
        sobj.setType(namespace + "Client_Card__c");
        sobj.setField("OwnerId", ownerIdForClientCard);
        sobj.setField(namespace + "Client__c", id);
        return sobj;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getLandline() {
        return landline;
    }

    public void setLandline(String landline) {
        this.landline = landline;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getRecordTypeId() {
        return recordTypeId;
    }

    public void setRecordTypeId(String recordTypeId) {
        this.recordTypeId = recordTypeId;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    @Override
    public String toString(){
        return "<Account: firstname=" + firstname + ", lastname=" + lastname + ", email=" + email +  ", landline=" + landline + ", mobile=" + mobile +">";
    }

    public boolean match(Account other) {
        if (StringUtils.isNotBlank(email) && email.equalsIgnoreCase(other.getEmail()))
            return true;
        else if (StringUtils.isNotBlank(mobile) && mobile.equalsIgnoreCase(other.getMobile()))
            return true;
        else return StringUtils.isNotBlank(landline) && mobile.equalsIgnoreCase(other.getLandline());

    }

    public int calculateMatchedScore(Account other){
        int score = 0;
        if(!StringUtils.isEmpty(mobile) && mobile.equalsIgnoreCase(other.getMobile())){
            score = 40;
        }

        if(!StringUtils.isEmpty(email) && email.equalsIgnoreCase(other.getEmail())){
            score += 30;
        }

        if(!StringUtils.isEmpty(landline) && landline.equalsIgnoreCase(other.getLandline())){
            score += 20;
        }
        return score;
    }

    public boolean isExistence() {
        return existence;
    }

    public void setExistence(boolean existence) {
        this.existence = existence;
    }

    public String getOwnerIdForClientCard() {
        return ownerIdForClientCard;
    }

    public void setOwnerIdForClientCard(String ownerIdForClientCard) {
        this.ownerIdForClientCard = ownerIdForClientCard;
    }
}
