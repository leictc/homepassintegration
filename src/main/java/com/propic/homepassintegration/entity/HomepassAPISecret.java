package com.propic.homepassintegration.entity;

import javax.persistence.*;

@Entity
@Table(name = "homepass_apisecret")
public class HomepassAPISecret {
    @Id
    @Column(name = "org_id")
    private String orgId;
    @Column
    private String secret;

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }
}
