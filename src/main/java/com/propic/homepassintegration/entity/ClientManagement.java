package com.propic.homepassintegration.entity;

import com.propic.homepassintegration.util.HomepassUtil;
import com.sforce.soap.partner.sobject.SObject;
import org.apache.commons.lang.StringUtils;

import java.util.Date;

public class ClientManagement {
    public static final String CLIENT_MANAGEMENT_SOBJECT = "Buyer_Management_1__c";
    public static final String ENQUIRY = "Enquiry";
    public static final String INSPECTION_ATTENDEE = "Inspection_Attendee";
    public static final String HOMEPASS = "HP";
    private InspectionAttendee inspectionAttendee;
    private String listingId;
    private String verb;
    private String sourceTag = HOMEPASS;
    private String recordTypeId;
    private String externalActivityId;
    private String displayName;
    private String activityTime;
    private String note;
    private boolean vendorComment=true;
    private String state;
    private String homepassUserId;
    private Account account;
    private String agentEmail;


    public InspectionAttendee getInspectionAttendee() {
        return inspectionAttendee;
    }

    public void setInspectionAttendee(InspectionAttendee inspectionAttendee) {
        this.inspectionAttendee = inspectionAttendee;
    }

    public String getListingId() {
        return listingId;
    }

    public void setListingId(String listingId) {
        this.listingId = listingId;
    }


    public String getVerb() {
        return verb;
    }

    public void setVerb(String verb) {
        this.verb = verb;
    }


    public String getSourceTag() {
        return sourceTag;
    }

    public void setSourceTag(String sourceTag) {
        this.sourceTag = sourceTag;
    }

    public boolean isValid(){
        return StringUtils.isNotEmpty(getRecordTypeId()) && getAccount()!= null && StringUtils.isNotEmpty(getAccount().getId()) && StringUtils.isNotEmpty(getExternalActivityId()) && StringUtils.isNotEmpty(getListingId());
    }

    public SObject toBuyerManagementSObject(final String namespace){
        SObject sobj = new SObject();
        sobj.setType(namespace+CLIENT_MANAGEMENT_SOBJECT);
        sobj.setField("RecordTypeId",getRecordTypeId());
        sobj.setField(namespace+"Buyer_Acc__c", getAccount().getId());
        sobj.setField(namespace+"External_System_ID__c", getExternalActivityId());
        sobj.setField(namespace+"Listing_1__c", getListingId());

        if(getInspectionAttendee() != null){
            sobj.setField(namespace+"Inspection_Attended__c", getInspectionAttendee().getId());
        }

        //if contract has been sent
        if(!StringUtils.isEmpty(getDisplayName())){
            if ("Sales Contract".equalsIgnoreCase(getDisplayName())) {
                sobj.setField(namespace + "Send_Contract__c", true);

                Date sfdate = HomepassUtil.convertUTCDateStringToDate(getActivityTime());
                if (sfdate == null) {
                    sfdate = new Date();
                }
                String localSentDateString = HomepassUtil.getDateTimeString(sfdate, HomepassUtil.getTimezoneId(getState()));
                Date sentDate = HomepassUtil.convertUTCDateStringToDate(localSentDateString);
                sobj.setField(namespace + "Contract_Sent_Date__c", sentDate);
            }
        }

        //if added note
        if(! StringUtils.isEmpty( getNote())){
            if (isVendorComment())
                sobj.setField(namespace+"Notes__c", getNote());
            else//else if (!orgId.equalsIgnoreCase("00D90000000vEAIEA2"))//Do not populate Private_Notes__c field if the org is mcgrath 00D90000000vEAIEA2, Private_Notes__c does not exist in mcgrath
                sobj.setField(namespace+"Private_Notes__c", getNote());


            Date sfdate = HomepassUtil.convertUTCDateStringToDate(getActivityTime());
            if(sfdate == null){
                sfdate = new Date();

            }

            String localNoteDateString = HomepassUtil.getDateTimeString(sfdate, HomepassUtil.getTimezoneId(getState()));
            Date noteDate = HomepassUtil.convertUTCDateStringToDate(localNoteDateString);
            sobj.setField(namespace+"Note_Created_Date__c",noteDate);
        }

        //for enquiry type
        if(StringUtils.isNotEmpty(getVerb()) && getVerb().equalsIgnoreCase("enquiry") && getSourceTag()!= null){
            sobj.setField(namespace+"Enquiry_Type__c",getSourceTag().replace(":","").replaceAll("\\s", ""));
        }

        if (StringUtils.isNotEmpty(getHomepassUserId()) /*&& !orgId.equalsIgnoreCase("00D90000000vEAIEA2") */){//Do not populate Homepass_User__c field if the org is mcgrath 00D90000000vEAIEA2, Homepass_User__c does not exist in mcgrath
            sobj.setField(namespace+"Homepass_User__c", getHomepassUserId());
        }
        return sobj;
    }

    public String getRecordTypeId() {
        return recordTypeId;
    }

    public void setRecordTypeId(String recordTypeId) {
        this.recordTypeId = recordTypeId;
    }

    public String getExternalActivityId() {
        return externalActivityId;
    }

    public void setExternalActivityId(String externalActivityId) {
        this.externalActivityId = externalActivityId;
    }


    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getActivityTime() {
        return activityTime;
    }

    public void setActivityTime(String activityTime) {
        this.activityTime = activityTime;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public boolean isVendorComment() {
        return vendorComment;
    }

    public void setVendorComment(boolean vendorComment) {
        this.vendorComment = vendorComment;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getHomepassUserId() {
        return homepassUserId;
    }

    public void setHomepassUserId(String homepassUserId) {
        this.homepassUserId = homepassUserId;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public String getAgentEmail() {
        return agentEmail;
    }

    public void setAgentEmail(String agentEmail) {
        this.agentEmail = agentEmail;
    }
}
