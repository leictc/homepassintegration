package com.propic.homepassintegration.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class OfficeTimeId implements Serializable {

    @Column(name="org_id")
    private String orgId;

    @Column(name="office_id")
    private String officeId;
    public OfficeTimeId(){

    }

    public OfficeTimeId(String orgId, String officeId){
        this.orgId = orgId;
        this.officeId = officeId;
    }

    public String getOrgId() {
        return orgId;
    }

    public String getOfficeId() {
        return officeId;
    }
}
