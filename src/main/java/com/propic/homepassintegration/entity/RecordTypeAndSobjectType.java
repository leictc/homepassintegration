package com.propic.homepassintegration.entity;

import java.util.Objects;

public class RecordTypeAndSobjectType {
    private String recordType;
    private String sobjectType;

    public RecordTypeAndSobjectType(String recordType, String sobjectType){
        this.recordType = recordType;
        this.sobjectType = sobjectType;
    }


    public String getRecordType() {
        return recordType;
    }

    public void setRecordType(String recordType) {
        this.recordType = recordType;
    }

    public String getSobjectType() {
        return sobjectType;
    }

    public void setSobjectType(String sobjectType) {
        this.sobjectType = sobjectType;
    }

    @Override
    public boolean equals(Object o){
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RecordTypeAndSobjectType recordTypeAndSobjectType = (RecordTypeAndSobjectType) o;
        return getRecordType().equalsIgnoreCase(recordTypeAndSobjectType.getRecordType()) && getSobjectType().equalsIgnoreCase(recordTypeAndSobjectType.getSobjectType());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getRecordType(), getSobjectType());
    }
}
