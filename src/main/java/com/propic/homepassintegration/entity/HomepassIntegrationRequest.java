package com.propic.homepassintegration.entity;


import io.netty.util.internal.StringUtil;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "homepass_integration_request")
public class HomepassIntegrationRequest {

    @Id
    @Column(name = "org_id")
    private String orgId;
    private String namespace;

    @Transient
    private String secret;

    @OneToMany(mappedBy="request", orphanRemoval = true, fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<IntegrationOffice> integrationOffices;

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public List<IntegrationOffice> getIntegrationOffices() {
        return integrationOffices;
    }

    public void setIntegrationOffices(List<IntegrationOffice> integrationOffices) {
        this.integrationOffices = integrationOffices;
    }

    public boolean isEligible() {
        return !StringUtil.isNullOrEmpty(orgId) && !integrationOffices.isEmpty();
    }

    @Override
    public String toString(){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(" orgId: "+orgId );
        stringBuilder.append(" namespace: "+namespace );
        return stringBuilder.toString();
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }
}
