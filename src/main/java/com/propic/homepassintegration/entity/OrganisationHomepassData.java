package com.propic.homepassintegration.entity;

import com.propic.homepassintegration.response.HomepassData;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class OrganisationHomepassData {
    private String orgId;
    private List<HomepassData> homepassDatas = new ArrayList<>();

    public OrganisationHomepassData(String orgId, List<HomepassData> homepassDatas){
        this.orgId = orgId;
        this.homepassDatas = homepassDatas;
    }

    public List<HomepassData> getHomepassDatas() {
        return homepassDatas;
    }

    public void setHomepassDatas(List<HomepassData> homepassDatas) {
        this.homepassDatas = homepassDatas;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public boolean isValid(){
        return StringUtils.isNotEmpty(orgId) && StringUtils.isNotBlank(orgId) && !homepassDatas.isEmpty();
    }
}
