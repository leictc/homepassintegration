package com.propic.homepassintegration.entity;

import com.propic.homepassintegration.util.HomepassUtil;
import com.sforce.soap.partner.sobject.SObject;

import java.util.Calendar;
import java.util.Date;

public class InspectionAttendee {


    public enum InspectionType{
        PRIVATE, PUBLIC
    }
    private static final String INSPECTION_STATUS_ACTIVE="Active";

    //private String namespace = "";

    //sf relevant inforamtions
    private String id;
    private String listingId;

    //inspection basic informations
    private String startDateTimeString;
    private Integer duration = 15;

    private InspectionType type = InspectionType.PUBLIC;

    //private SFActivity sfActivity;

    //attendee datetime
    private String attendedDateTimeSFUTCString;

    /*
    public SFInspectionAttendee(SFActivity sfActivity){
        this.sfActivity = sfActivity;

    }*/



    public String getStartDateTimeString() {
        return startDateTimeString;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setStartDateTimeString(String startDateTimeString) {
        this.startDateTimeString = startDateTimeString;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    /*
    public String getState() {
        return sfActivity.getState();
    }*/



    public SObject toSObject(final String namespace){
        SObject sobject=new SObject();

        if(this.id!=null) {
            sobject.setId(this.id);
        }

        this.duration = (this.duration % 15) * 15;
        if( this.duration == 0){
            this.duration = 15;
        }

        //get UTC start datetime
        Date inspectionUTCStartDate = HomepassUtil.convertUTCDateStringToDate(this.startDateTimeString);
        Calendar localstartDate = Calendar.getInstance();
        localstartDate.setTime(inspectionUTCStartDate);
        Calendar localendDate = Calendar.getInstance();
        localendDate.setTime(localstartDate.getTime());
        localendDate.add(Calendar.MINUTE, this.duration);

        sobject.setType(namespace + "Inspection__c");
        sobject.setField(namespace + "Listing__c", this.listingId);

        sobject.setField(namespace + "Inspection_DateTime__c",localstartDate);
        sobject.setField(namespace + "Inspection_End_DateTime__c",localendDate);
        sobject.setField(namespace + "Inspection_Duration__c",""+this.duration);

        sobject.setField(namespace + "Inspection_Type__c",this.type);
        sobject.setField(namespace + "Status__c", INSPECTION_STATUS_ACTIVE);
        return sobject;
    }

    public String toConditionString(String namespace){



        return  namespace+"Listing__c='"+listingId+"' ";


    }

    public String getId() {
        return id;
    }

    public String getListingId() {
        return listingId;
    }



    public void setId(String id) {
        this.id = id;
    }

    public void setListingId(String listingId) {
        this.listingId = listingId;
    }


    public String getAttendedDateTimeSFUTCString() {
        return attendedDateTimeSFUTCString;
    }

    public void setAttendedDateTimeSFUTCString(String attendedDateTimeSFUTCString) {
        this.attendedDateTimeSFUTCString = attendedDateTimeSFUTCString;
    }

    /*
    public SFActivity getSfActivity() {
        return sfActivity;
    }

    public void setSfActivity(SFActivity sfActivity) {
        this.sfActivity = sfActivity;
    }*/



    public InspectionType getInspectionType() {
        return type;
    }



    public void setInspectionType(InspectionType type) {
        this.type = type;
    }


}
