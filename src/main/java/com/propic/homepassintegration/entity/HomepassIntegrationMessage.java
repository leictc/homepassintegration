package com.propic.homepassintegration.entity;

import io.netty.util.internal.StringUtil;

import java.util.ArrayList;
import java.util.List;

public class HomepassIntegrationMessage {

    private String orgId;
    private String namespace;
    private String homepassAPISecret;
    private List<Object> residentialOfficeIds = new ArrayList<>();
    private List<Object> projectOfficeIds = new ArrayList<>();


    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }


    public String getHomepassAPISecret() {
        return homepassAPISecret;
    }

    public void setHomepassAPISecret(String homepassAPISecret) {
        this.homepassAPISecret = homepassAPISecret;
    }

    public List<Object> getResidentialOfficeIds() {
        return residentialOfficeIds;
    }

    public void setResidentialOfficeIds(List<Object> residentialOfficeIds) {
        this.residentialOfficeIds = residentialOfficeIds;
    }

    public List<Object> getProjectOfficeIds() {
        return projectOfficeIds;
    }

    public void setProjectOfficeIds(List<Object> projectOfficeIds) {
        this.projectOfficeIds = projectOfficeIds;
    }

    public boolean isEligible() {
        return !StringUtil.isNullOrEmpty(orgId) && !StringUtil.isNullOrEmpty(homepassAPISecret) && ( !residentialOfficeIds.isEmpty()||!projectOfficeIds.isEmpty());
    }

    @Override
    public String toString(){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(" orgId: "+orgId );
        stringBuilder.append(" namespace: "+namespace );
        stringBuilder.append(" homepassAPISecret: "+homepassAPISecret );
        /*stringBuilder.append(" residentialOfficeIds: "+residentialOfficeIds.toArray() );
        stringBuilder.append(" projectOfficeIds: "+projectOfficeIds.toArray() );*/
        return stringBuilder.toString();
    }
}
