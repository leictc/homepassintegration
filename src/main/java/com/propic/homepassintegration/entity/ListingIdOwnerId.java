package com.propic.homepassintegration.entity;

import org.apache.commons.lang.StringUtils;

public class ListingIdOwnerId {
    private String listingId;
    private String ownerId;

    public ListingIdOwnerId(String listingId, String ownerId){
        this.listingId = listingId;
        this.ownerId = ownerId;
    }

    public String getListingId() {
        return listingId;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public boolean isValid(){
        return StringUtils.isNotEmpty(getListingId()) && StringUtils.isNotEmpty(getOwnerId());
    }
}
