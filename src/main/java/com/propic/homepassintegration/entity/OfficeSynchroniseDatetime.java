package com.propic.homepassintegration.entity;


import javax.persistence.*;

@Entity
@Table(name = "office_synchronise_datetime")
public class OfficeSynchroniseDatetime {
    @EmbeddedId
    private OfficeTimeId id;

    @Column(name = "last_synchronise_datetime")
    private String lastSynchroniseDatetime;

    public OfficeSynchroniseDatetime(){

    }

    public OfficeSynchroniseDatetime(OfficeTimeId id, String lastSynchroniseDatetime){
        this.id = id;
        this.lastSynchroniseDatetime = lastSynchroniseDatetime;
    }

    public String getLastSynchroniseDatetime() {
        return lastSynchroniseDatetime;
    }

    public void setLastSynchroniseDatetime(String lastSynchroniseDatetime) {
        this.lastSynchroniseDatetime = lastSynchroniseDatetime;
    }

    public OfficeTimeId getId() {
        return id;
    }

    public void setId(OfficeTimeId id) {
        this.id = id;
    }
}
