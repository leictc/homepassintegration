package com.propic.homepassintegration.entity;

public class Inspection {
    public static final String TYPE_PUBLIC = "public";
    public static final String TYPE_PRIVATE = "private";

    private String startUTCDateTimeString;

    //minutes
    private Integer duration;

    //private or public
    private String type = TYPE_PUBLIC;

    //for choosing timezone of the state: nsw, vic....
    //private String state;

    public String getStartUTCDateTimeString() {
        return startUTCDateTimeString;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setStartUTCDateTimeString(String startUTCDateTimeString) {
        this.startUTCDateTimeString = startUTCDateTimeString;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Inspection [startUTCDateTimeString=" + startUTCDateTimeString
                + ", duration=" + duration + "]";
    }
}
