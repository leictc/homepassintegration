package com.propic.homepassintegration.entity;

import org.apache.commons.lang.StringUtils;

import java.util.List;

public class Activity {
    public static final String CHECKIN = "checkin";
    public static final String ADD = "add";
    public static final String SEND = "send";
    public static final String ENQUIRY = "enquiry";

    private List<Client> clientList;
    private String agentId;
    private String adsId;
    private String activityTime;
    private String homepassActivityId;
    private String homepassAgentId;
    private String verb;
    private String officeId;

    private String note;
    private String mediaType;
    private String displayName;
    private String documentURL;
    private String propAddress;
    private String activityType="";
    private String agentEmail;
    private boolean vendorComment = true;
    private ClientManagementType type;
    private String externalActivityId;


    public List<Client> getClientList() {
        return clientList;
    }

    public void setClientList(List<Client> clientList) {
        this.clientList = clientList;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getAdsId() {
        return adsId;
    }

    public void setAdsId(String adsId) {
        this.adsId = adsId;
    }

    public String getActivityTime() {
        return activityTime;
    }

    public void setActivityTime(String activityTime) {
        this.activityTime = activityTime;
    }

    public String getHomepassActivityId() {
        return homepassActivityId;
    }

    public void setHomepassActivityId(String homepassActivityId) {
        this.homepassActivityId = homepassActivityId;
    }

    public String getHomepassAgentId() {
        return homepassAgentId;
    }

    public void setHomepassAgentId(String homepassAgentId) {
        this.homepassAgentId = homepassAgentId;
    }

    public String getVerb() {
        return verb;
    }

    public void setVerb(String verb) {
        this.verb = verb;
    }

    public String getOfficeId() {
        return officeId;
    }

    public void setOfficeId(String officeId) {
        this.officeId = officeId;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getMediaType() {
        return mediaType;
    }

    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getDocumentURL() {
        return documentURL;
    }

    public void setDocumentURL(String documentURL) {
        this.documentURL = documentURL;
    }

    public String getPropAddress() {
        return propAddress;
    }

    public void setPropAddress(String propAddress) {
        this.propAddress = propAddress;
    }

    public String getActivityType() {
        return activityType;
    }

    public void setActivityType(String activityType) {
        this.activityType = activityType;
    }

    public String getAgentEmail() {
        return agentEmail;
    }

    public void setAgentEmail(String agentEmail) {
        this.agentEmail = agentEmail;
    }

    public boolean isVendorComment() {
        return vendorComment;
    }

    public void setVendorComment(boolean vendorComment) {
        this.vendorComment = vendorComment;
    }

    public ClientManagementType getType() {
        return type;
    }

    public void setType(ClientManagementType type) {
        this.type = type;
    }

    public boolean isValid(){
        return StringUtils.isNotEmpty(getActivityTime()) && !getClientList().isEmpty() && StringUtils.isEmpty(getPropAddress());
    }

    public String getExternalActivityId() {
        return externalActivityId;
    }

    public void setExternalActivityId(String externalActivityId) {
        this.externalActivityId = externalActivityId;
    }
}
