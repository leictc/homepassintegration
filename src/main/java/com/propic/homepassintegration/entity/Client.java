package com.propic.homepassintegration.entity;

import com.sforce.soap.partner.sobject.SObject;
import org.apache.commons.lang.StringUtils;

public class Client {

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public SObject toSObject() {
        return null;
    }

    public String getLandline() {
        return landline;
    }

    public String getLastname() {
        return lastname;
    }

    public Address getAddress() {
        return address;
    }

    public enum Role{
        TARGET, ACTOR, BOTH
    }

    private String id;
    private Role role;
    private String ownerId;
    private String firstname;
    private String lastname="";
    private String email="";
    private String mobile="";
    private String landline="";
    private Address address;
    private String recordTypeId;
    private String namespace = "";

    public void setId(String id) {
        this.id = id;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public void setLandline(String landline) {
        this.landline = landline;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public void setRecordTypeId(String recordTypeId) {
        this.recordTypeId = recordTypeId;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public String getEmail(){
        return this.email;
    }

    public String getMobile() {
        return this.mobile;
    }

    public String getFirstName() {
        return this.firstname;
    }

    public boolean isValid(){
        if (StringUtils.isEmpty(getEmail()) && StringUtils.isEmpty(getMobile()) && StringUtils.isEmpty(getLandline()))
            return false;
        else if (StringUtils.isEmpty(getLastname()))
            return false;
        else if (!getRole().equals(Role.TARGET) && ! getRole().equals(Role.BOTH))
            return false;
        return true;
    }
}
