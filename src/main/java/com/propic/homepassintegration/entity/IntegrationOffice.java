package com.propic.homepassintegration.entity;


import org.joda.time.DateTime;

import javax.persistence.*;

@Entity
@Table(name = "integration_office")
public class IntegrationOffice {

    @Id
    @Column(name = "office_id")
    private String officeId;

    private boolean project = false;


    @ManyToOne
    @JoinColumn(name="org_id", nullable=false)
    private HomepassIntegrationRequest request;

    public String getOfficeId() {
        return officeId;
    }

    public void setOfficeId(String officeId) {
        this.officeId = officeId;
    }

    public HomepassIntegrationRequest getRequest() {
        return request;
    }

    public void setRequest(HomepassIntegrationRequest request) {
        this.request = request;
    }

    public boolean isProject() {
        return project;
    }

    public void setProject(boolean project) {
        this.project = project;
    }
}
