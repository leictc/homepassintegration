package com.propic.homepassintegration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
//@EnableScheduling
public class HomepassIntegrationApplication {

	public static void main(String[] args) {
		SpringApplication.run(HomepassIntegrationApplication.class, args);
	}

	/*
	@Configuration
	@Profile("!test")
	@EnableScheduling
	static class SchedulingConfiguration {

	}*/

}
