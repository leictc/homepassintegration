package com.propic.homepassintegration.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class AttachedTo {

    @JsonProperty("@type")
    private String type;

    @JsonProperty("@id")
    private String id;

    @JsonProperty("url")
    private String url;

    @JsonProperty("hp:addr")
    private Address address;

    @JsonProperty("hp:ref")
    private String reference;

    @JsonProperty("hp:xRefs")
    private List<Reference> referenceList;

    @JsonProperty("mediaType")
    private String mediaType;
    @JsonProperty("displayName")
    private String displayName;

    public String getMediaType() {
        return mediaType;
    }

    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public List<Reference> getReferenceList() {
        return referenceList;
    }

    public void setReferenceList(List<Reference> referenceList) {
        this.referenceList = referenceList;
    }

}
