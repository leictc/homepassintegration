package com.propic.homepassintegration.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Item {
    @JsonProperty("@id")
    private String id;

    @JsonProperty("@type")
    private String type;

    @JsonProperty("published")
    private String published;

    @JsonProperty("verb")
    private String verb;

    @JsonProperty("actor")
    private Person actor;

    @JsonProperty("target")
    private Person target;

    @JsonProperty("object")
    private Object object;

    //extended by andy yang 20-12-2016
    @JsonProperty("attachedTo")
    private AttachedTo attachedTo;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPublished() {
        return published;
    }

    public void setPublished(String published) {
        this.published = published;
    }

    public String getVerb() {
        return verb;
    }

    public void setVerb(String verb) {
        this.verb = verb;
    }

    public Person getActor() {
        return actor;
    }

    public void setActor(Person actor) {
        this.actor = actor;
    }

    public Person getTarget() {
        return target;
    }

    public void setTarget(Person target) {
        this.target = target;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }

    public AttachedTo getAttachedTo() {
        return attachedTo;
    }

    public void setAttachedTo(AttachedTo attachedTo) {
        this.attachedTo = attachedTo;
    }
}

