package com.propic.homepassintegration.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.StringJoiner;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Address {

    private static final String COUNTRY_NAME_AU = "AU";
    private static final String COUNTRY_NAME_AUS = "AUS";
    private static final String COUNTRY_NAME_AUSTRALIA = "AUSTRALIA";

    @JsonProperty("street-address")
    private String streetAddress;

    @JsonProperty("locality")
    private String locality;

    @JsonProperty("region")
    private String region;

    @JsonProperty("postal-code")
    private String postalCode;

    @JsonProperty("country-name")
    private String countryName;

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    @Override
    public String toString() {
        StringJoiner strJoiner = new StringJoiner(" ");
        if (null != streetAddress)
            strJoiner.add(streetAddress);
        if (null != locality)
            strJoiner.add(locality);
        if (null != region)
            strJoiner.add(region);
        if (null != postalCode)
            strJoiner.add(postalCode);
        // append the other country name except australia
        if (null != countryName && !isAustralia(countryName))
            strJoiner.add(countryName);
        return strJoiner.toString();
    }

    private boolean isAustralia(String countryName){
        return COUNTRY_NAME_AUS.equalsIgnoreCase(countryName) || COUNTRY_NAME_AUSTRALIA.equalsIgnoreCase(countryName) || COUNTRY_NAME_AU.equalsIgnoreCase(countryName);
    }
}
