package com.propic.homepassintegration.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.propic.homepassintegration.entity.ClientManagementType;


@JsonIgnoreProperties(ignoreUnknown = true)
public class HomepassData {

    private String officeId;

    @JsonProperty("code")
    private Integer code;

    @JsonProperty("data")
    private Data data;

    private ClientManagementType type = ClientManagementType.RESIDENTIAL;


    public String getOfficeId() {
        return officeId;
    }

    public void setOfficeId(String officeId) {
        this.officeId = officeId;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public ClientManagementType getType() {
        return type;
    }

    public void setType(ClientManagementType type) {
        this.type = type;
    }
}
