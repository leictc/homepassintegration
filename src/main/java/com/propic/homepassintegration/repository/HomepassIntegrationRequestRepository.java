package com.propic.homepassintegration.repository;

import com.propic.homepassintegration.entity.HomepassIntegrationRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HomepassIntegrationRequestRepository extends JpaRepository<HomepassIntegrationRequest, String> {
}
