package com.propic.homepassintegration.repository;

import com.propic.homepassintegration.entity.HomepassAPISecret;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HomepassAPISecretRepository extends JpaRepository<HomepassAPISecret, String> {
}
