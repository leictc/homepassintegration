package com.propic.homepassintegration.repository;

import com.propic.homepassintegration.entity.IntegrationOffice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IntegrationOfficeRepository  extends JpaRepository<IntegrationOffice, String> {
}
