package com.propic.homepassintegration.repository;

import com.propic.homepassintegration.entity.OfficeSynchroniseDatetime;
import com.propic.homepassintegration.entity.OfficeTimeId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OfficeSynchroniseDatetimeRepository  extends JpaRepository<OfficeSynchroniseDatetime, OfficeTimeId> {
}
