package com.propic.homepassintegration.util;

import com.propic.homepassintegration.entity.*;
import com.propic.homepassintegration.response.HomepassData;
import com.propic.homepassintegration.response.Item;
import com.propic.homepassintegration.response.Person;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.*;

public class HomepassUtil {
    private static final String DEFAULT_TIMEZONE_ID = "Australia/NSW";
    private static Map<String,String> timezoneMap =new HashMap<>();
    static{
        timezoneMap.put("wa","Australia/West");    //+800
        timezoneMap.put("eucla","Australia/Eucla");   //+8:45
        timezoneMap.put("sa","Australia/South" );  //+9:30
        timezoneMap.put("nt","Australia/South" );  //+9:30
        timezoneMap.put("nsw","Australia/NSW");//+10:00
        timezoneMap.put("act","Australia/NSW");//+10:00
        timezoneMap.put("tas","Australia/NSW");//+10:00
        timezoneMap.put("vic","Australia/NSW");//+10:00
        timezoneMap.put("qld","Australia/NSW");//+10:00
        timezoneMap.put("jbt", "Australia/NSW");//+10:00
        timezoneMap.put("gmt","Etc/GMT"); //+0
    }

    private static final Logger LOG = LogManager.getLogger(HomepassUtil.class.getName());

    public static Optional<HomepassIntegrationMessage> toHomepassIntegrationMessage(final String jsonStr){
        LOG.info("Converting json to homepass integration message");
        final HomepassIntegrationMessage message = new HomepassIntegrationMessage();
        try {
            JSONObject obj = new JSONObject(jsonStr);
            message.setOrgId(obj.getString("orgId"));
            message.setNamespace(obj.getString("namespace"));
            message.setHomepassAPISecret(obj.getString("homepassAPISecret"));
            message.setResidentialOfficeIds(obj.getJSONArray("officeIdList").toList());
            message.setProjectOfficeIds(obj.getJSONArray("projectOfficeIdList").toList());
        }
        catch (JSONException e){
            LOG.error("Fail to convert homepass integration message from :"+jsonStr+" due to "+e);
            return Optional.empty();
        }
        if (message.isEligible())
            return Optional.of(message);
        else
            return Optional.empty();
    }

    public static Optional<HomepassIntegrationRequest> toHomepassIntegrationRequest(final String jsonStr){
        LOG.info("Converting json to homepass integration request, json is: "+jsonStr);
        final HomepassIntegrationRequest request= new HomepassIntegrationRequest();
        try {
            JSONObject obj = new JSONObject(jsonStr);
            request.setOrgId(obj.getString("orgId"));
            request.setNamespace(obj.getString("namespace"));
            request.setSecret(obj.getString("homepassAPISecret"));
            List<IntegrationOffice> integrationOffices = new ArrayList<>();
            obj.getJSONArray("officeIdList").toList().forEach(o -> {

                IntegrationOffice integrationOffice = new IntegrationOffice();
                integrationOffice.setOfficeId((String)o);
                integrationOffice.setRequest(request);
                integrationOffice.setProject(false);
                integrationOffices.add(integrationOffice);
            });
            obj.getJSONArray("projectOfficeIdList").toList().forEach(o -> {

                IntegrationOffice integrationOffice = new IntegrationOffice();
                integrationOffice.setOfficeId((String)o);
                integrationOffice.setRequest(request);
                integrationOffice.setProject(true);
                integrationOffices.add(integrationOffice);
            });
            request.setIntegrationOffices(integrationOffices);
        }
        catch (JSONException e){
            LOG.error("Fail to convert homepass integration request from :"+jsonStr+" due to "+e);
            return Optional.empty();
        }
        if (request.isEligible())
            return Optional.of(request);
        else
            return Optional.empty();
    }

    public static String toUtcDateString(Date d) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        TimeZone tz = TimeZone.getTimeZone("UTC");
        dateFormat.setTimeZone(tz);

        try {
            return dateFormat.format(d);
        } catch (Exception e) {
            LOG.error("Fail to convert date "+d+" to date format string");
            return null;
        }
    }

    public static String toCurrentUtcDatetimeString(){
        return toUtcDateString(DateTime.now().toDate());
    }









    public static Date convertUTCDateStringToDate(String sfDateString){

        return convertDateStringToDate(sfDateString,"yyyy-MM-dd'T'HH:mm:ss","UTC");

    }

    private static Date convertDateStringToDate(String srcDate, String srcDateFormat,String srcTimeZone) {
        SimpleDateFormat reaFormat = new SimpleDateFormat(srcDateFormat);
        TimeZone tz = TimeZone.getTimeZone(srcTimeZone);
        reaFormat.setTimeZone(tz);
        try {
            return reaFormat.parse(srcDate);

        } catch (Exception e) {
            LOG.error("Fail to parse "+srcDate+" to date due to "+e);
            return null;
        }
    }

    public static String getDateTimeString(Date date, String tzId) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        dateFormat.setTimeZone(TimeZone.getTimeZone(tzId));

        return dateFormat.format(date);
    }

    public static String getTimezoneId(String state){

        String tzId = null;
        if(! StringUtils.isEmpty( state )){
            tzId = timezoneMap.get(state.toLowerCase());
        }

        if(StringUtils.isEmpty( tzId )){
            tzId = DEFAULT_TIMEZONE_ID;
        }

        return tzId;
    }



    public static boolean isSameAccount(Account account1, Account account2){
        return false;
    }



}
