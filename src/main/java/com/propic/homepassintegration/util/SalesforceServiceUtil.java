package com.propic.homepassintegration.util;

import com.propic.homepassintegration.entity.*;
import com.propic.homepassintegration.response.HomepassData;
import com.propic.homepassintegration.response.Item;
import com.propic.homepassintegration.response.Person;
import com.sforce.soap.partner.PartnerConnection;
import com.sforce.soap.partner.QueryResult;
import com.sforce.ws.ConnectionException;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class SalesforceServiceUtil {

    private static final Logger LOG = LogManager.getLogger(SalesforceServiceUtil.class.getName());

    public static Map<String, List<Activity>> parseHomepassData(final List<OrganisationHomepassData> organisationHomepassDataList) {
        final Map<String, List<Activity>> orgActivitiesMap = new HashMap<>();

        organisationHomepassDataList.forEach(organisationHomepassData -> {
            final List<Activity> activities = new ArrayList<>();
            organisationHomepassData.getHomepassDatas().forEach(homepassData -> {
                activities.addAll(toActivities(homepassData));
            });
            if (!activities.isEmpty())
                orgActivitiesMap.put(organisationHomepassData.getOrgId(), activities);
        });

        return orgActivitiesMap;
    }

    private static List<Activity> toActivities(final HomepassData homepassData) {
        LOG.info("Converting homepass data to Activities");
        List<Activity> activities = new ArrayList<>();
        homepassData.getData().getItems().forEach(item -> {
            activities.add(toActivity(item, homepassData.getOfficeId(), homepassData.getType()));

        });
        return activities;
    }


    private static Activity toActivity(final Item item, final String officeId, final ClientManagementType type) {
        Activity activity = new Activity();
        List<Client> clientList = new ArrayList<>();

        //set verb
        activity.setVerb(item.getVerb());

        //set activity time
        activity.setActivityTime(item.getPublished());

        //set homepass activity id
        activity.setHomepassActivityId(item.getId());

        //set officeId
        activity.setOfficeId(officeId);

        activity.setType(type);

        //map actor
        Person actor = item.getActor();
        if (actor != null) {
            if (actor.getType() != null && actor.getType().toLowerCase().contains("agent")) { //actor is an agent
                //set agentId, homepassAgentId
                agentMapping(activity, actor);
            } else { //actor is an client
                //set Client
                Client client = getClientMapping(actor, Client.Role.ACTOR);
                clientList.add(client);
            }
        }

        //map target
        Person target = item.getTarget();
        if (target != null) {
            if (target.getType() != null && target.getType().toLowerCase().contains("agent")) { //target is an agent
                //set agentId, homepassAgentId
                agentMapping(activity, target);
            } else { //actor is an client
                //set Client
                Client client = getClientMapping(target, Client.Role.TARGET);
                clientList.add(client);
            }
        }

        if (clientList.size() > 1) { //if there are two clients.
            Client client1 = clientList.get(0);
            Client client2 = clientList.get(1);
            if (client1.getEmail().equalsIgnoreCase(client2.getEmail()) //if 2client have same email,mobile, firstName, assuem those two are same person
                    && client1.getMobile().equalsIgnoreCase(client2.getMobile())
                    && client1.getFirstName().equalsIgnoreCase(client2.getFirstName())
            ) {
                client1.setRole(Client.Role.BOTH);
                clientList.clear();
                clientList.add(client1);
            }
        }
        activity.setClientList(clientList);
        return activity;
    }

    private static void agentMapping(Activity activity, Person person) {
        String agentId = person.getRef();
        if (agentId == null || agentId.equals("")) {
            if (person.getxRef() != null && person.getxRef().size() > 0) {
                agentId = person.getxRef().get(0).getRef();

            }
        }

        if (agentId != null && !agentId.isEmpty())
            activity.setAgentId(agentId);
        if (person.getPersonDetail() != null)
            activity.setAgentEmail(person.getPersonDetail().getEmail());

        activity.setHomepassAgentId(person.getId());
    }

    private static Client getClientMapping(Person person, Client.Role role) {
        Client client = new Client();

        client.setRole(role);
        if (person.getAddress() != null)
            client.setAddress(mapAddress(person.getAddress()));

        if (person.getPersonDetail() != null) {
            if (person.getPersonDetail().getEmail() != null)
                client.setEmail(person.getPersonDetail().getEmail().trim());

            if (person.getPersonDetail().getFirstName() != null)
                client.setFirstname(person.getPersonDetail().getFirstName().trim());

            if (person.getPersonDetail().getLastName() != null) {
                client.setLastname(person.getPersonDetail().getLastName().trim());
            }

            if (person.getPersonDetail().getLandline() != null)
                client.setLandline(person.getPersonDetail().getLandline().trim());

            if (person.getPersonDetail().getMobile() != null)
                client.setMobile(person.getPersonDetail().getMobile().trim());
        }
        return client;
    }

    private static Address mapAddress(com.propic.homepassintegration.response.Address homepassAddress) {
        Address mappedAddress = new Address();

        if (homepassAddress.getStreetAddress() != null)
            mappedAddress.setStreet(homepassAddress.getStreetAddress());

        if (homepassAddress.getLocality() != null)
            mappedAddress.setCity(homepassAddress.getLocality());

        if (homepassAddress.getRegion() != null)
            mappedAddress.setState(homepassAddress.getRegion());

        if (homepassAddress.getPostalCode() != null)
            mappedAddress.setPostcode(homepassAddress.getPostalCode());

        if (homepassAddress.getCountryName() != null)
            mappedAddress.setCountry(homepassAddress.getCountryName());

        return mappedAddress;
    }

    public static String createAccountQuery(final List<Account> accounts, final String namespace) {
        StringBuilder emailConditions = new StringBuilder(" PersonEmail" + " IN (");
        StringBuilder mobileConditions = new StringBuilder(" PersonMobilePhone IN (");
        StringBuilder landlineConditions = new StringBuilder(" PersonHomePhone IN (");
        Set<String> emails = new HashSet<>();
        Set<String> mobiles = new HashSet<>();
        Set<String> landlines = new HashSet<>();

        for (Account account : accounts) {
            if (StringUtils.isNotEmpty(account.getEmail())) {
                emails.add(account.getEmail());
            }
            if (StringUtils.isNotEmpty(account.getMobile())) {
                mobiles.add(account.getMobile());
            }
            if (StringUtils.isNotEmpty(account.getLandline())) {
                landlines.add(account.getLandline());
            }
        }

        if (emails.isEmpty()) {
            emailConditions = new StringBuilder();
        } else {
            for (String email : emails) {
                emailConditions.append("'").append(email).append("' ,");
            }
            emailConditions = new StringBuilder(emailConditions.substring(0, emailConditions.length() - 1));
            emailConditions.append(")");
        }

        if (mobiles.isEmpty()) {
            mobileConditions = new StringBuilder();
        } else {
            for (String mobile : mobiles) {
                mobileConditions.append("'").append(mobile).append("' ,");
            }
            mobileConditions = new StringBuilder(mobileConditions.substring(0, mobileConditions.length() - 1));
            mobileConditions.append(")");
        }

        if (landlines.isEmpty()) {
            landlineConditions = new StringBuilder();
        } else {
            for (String landline : landlines) {
                landlineConditions.append("'").append(landline).append("' ,");
            }
            landlineConditions = new StringBuilder(landlineConditions.substring(0, landlineConditions.length() - 1));
            landlineConditions.append(")");
        }

        String conditions = landlineConditions.toString();

        if (StringUtils.isNotEmpty(emailConditions.toString())) {
            if (StringUtils.isEmpty(conditions))
                conditions = emailConditions.toString();
            else
                conditions += " or " + emailConditions;
        }
        if (StringUtils.isNotEmpty(mobileConditions.toString())) {
            if (StringUtils.isEmpty(conditions))
                conditions = mobileConditions.toString();
            else
                conditions += " or " + mobileConditions;
        }

        if (StringUtils.isNotEmpty(conditions)) {
            conditions = " where " + conditions;
        }


        String queryString = "select Id, FirstName, LastName, PersonEmail,PersonMobilePhone, PersonHomePhone, "
                + " (select Id, OwnerId from " + namespace + "Client_Cards__r) "
                + " from Account "
                + conditions + " order by createdDate desc limit 1000";

        LOG.info("Quering account: " + queryString);
        return queryString;
    }

    public static String createAddressListingIdOwnerIdMapQuery(final List<Activity> clientManagementActivities, final String namespace) {
        final String statusConditions = namespace + "Status__c IN ('Exchanged','Listed', 'Under Offer', 'Offer and Acceptance') ";
        String addressConditions = namespace + "Property_Address__c" + " IN (";
        int count = 0;
        Set<String> addresses = new HashSet<>();
        for (Activity activity : clientManagementActivities) {
            String address = activity.getPropAddress();
            if (StringUtils.isEmpty(address))
                continue;
            if (!addresses.add(address)) {
                continue;
            }

            if (count == 0) {
                addressConditions = addressConditions + "'" + address + "'";
            } else {
                addressConditions = addressConditions + ", '" + address + "'";
            }
            count++;
        }
        addressConditions = addressConditions + ")";

        String queryString = "select "
                + "Id, "
                + "OwnerId, "
                + namespace + "Property_Address__c "
                + "from "
                + namespace + "Appraisal_Listing__c "
                + "where "
                + statusConditions
                + "  AND "
                + addressConditions
                + " order by createdDate desc";
        LOG.debug("Query to fetch listing id and owner id: " + queryString);
        return queryString;
    }

    public static Optional<ClientManagement> toClientManagement(final Activity activity, final Map<RecordTypeAndSobjectType, String> recordTypeAndSobjectTypeIdMap, final Map<String,ListingIdOwnerId>  addressListingIdOwnerIdMap, final Map<String,String> listingIdOwnerIdMap ){
        LOG.info("Converting activity "+activity+" to client management");

        if (addressListingIdOwnerIdMap.isEmpty()){
            LOG.error("No listing id and owner id match up any addresses");
            return Optional.empty();
        }
        if (!activity.isValid()){
            LOG.error("Activity is invalid");
            return Optional.empty();
        }
        ListingIdOwnerId listingIdOwnerId = addressListingIdOwnerIdMap.get(activity.getPropAddress());
        String ownerIdForClientCard;
        if (listingIdOwnerId == null){
            LOG.error("Activity can't find matched listing id and owner id");
            return Optional.empty();
        }
        else{
            ownerIdForClientCard = listingIdOwnerIdMap.get(listingIdOwnerId.getListingId());
        }

        ClientManagement clientManagement = new ClientManagement();
        clientManagement.setActivityTime(activity.getActivityTime());
        clientManagement.setDisplayName(activity.getDisplayName());
        clientManagement.setListingId(listingIdOwnerId.getListingId());
        clientManagement.setExternalActivityId( ClientManagement.HOMEPASS + activity.getExternalActivityId());
        clientManagement.setAgentEmail(activity.getAgentEmail());

        String recordTypeId = null;
        if (Activity.CHECKIN.equalsIgnoreCase(activity.getVerb())){
            recordTypeId = recordTypeAndSobjectTypeIdMap.get(new RecordTypeAndSobjectType(ClientManagement.INSPECTION_ATTENDEE,  ClientManagement.CLIENT_MANAGEMENT_SOBJECT));
            InspectionAttendee inspectionAttendee = new InspectionAttendee();
            inspectionAttendee.setAttendedDateTimeSFUTCString( activity.getActivityTime()  );
            inspectionAttendee.setInspectionType(InspectionAttendee.InspectionType.PUBLIC);
        }
        else if (Activity.ENQUIRY.equalsIgnoreCase(activity.getVerb())){
            recordTypeId = recordTypeAndSobjectTypeIdMap.get(new RecordTypeAndSobjectType(ClientManagement.ENQUIRY,  ClientManagement.CLIENT_MANAGEMENT_SOBJECT));
        }
        clientManagement.setRecordTypeId(recordTypeId);

        Account account = new Account();
        String accountRecordTypeId = recordTypeAndSobjectTypeIdMap.get(new RecordTypeAndSobjectType(Account.RECORD_TYPE, Account.SOBJECT));
        account.setRecordTypeId(accountRecordTypeId);
        activity.getClientList().stream().filter(Client::isValid).forEach(client -> {
            LOG.info("Client "+client);
            if (StringUtils.isEmpty(client.getFirstName())){
                account.setFirstname(".");
            }
            else{
                account.setFirstname(client.getFirstName());
            }
            account.setLastname(client.getLastname());
            if(StringUtils.isNotEmpty(client.getEmail())){
                String  email = client.getEmail().replaceAll("\\s", "");
                if(email.endsWith(".") && !email.equals(".")){
                    email = email.substring(0,email.length() - 1);
                }
                account.setEmail(email);
            }
            account.setMobile(client.getMobile());
            account.setLandline(client.getLandline());
            account.setAddress(client.getAddress());
            account.setOwnerId(listingIdOwnerId.getOwnerId());
            account.setOwnerIdForClientCard(ownerIdForClientCard);
            clientManagement.setAccount(account);
        });

        if (clientManagement.isValid()){
            return Optional.of(clientManagement);
        }
        return Optional.empty();

    }

    public static Map<String, String> getEmailUserIdMap(final PartnerConnection conn, final List<Activity> activities){
        List<String> emails = new ArrayList<>();
        activities.stream().filter(activity -> StringUtils.isNotEmpty(activity.getAgentEmail())).forEach(activity -> emails.add(activity.getAgentEmail()));
        Map<String, String> emailUserIdMap = new HashMap<>();
        toUserEmailQuery(emails).ifPresent(queryString -> {
            QueryResult queryResult = null;

            try {
                queryResult = conn.query(queryString);
            } catch (ConnectionException e) {
                LOG.error("Fail to query the user with sender email due to "+e);
            }

            for (int i=0; i< queryResult.getRecords().length;i++){
                String email= (String) queryResult.getRecords()[i].getField("Email");
                String senderEmail = (String) queryResult.getRecords()[i].getField("SenderEmail");
                String id = (String) queryResult.getRecords()[i].getField("Id");
                if (!StringUtils.isEmpty(senderEmail) && emailUserIdMap.get(senderEmail)== null) {
                    emailUserIdMap.put(senderEmail, id);
                }
                else if (!StringUtils.isEmpty(email) && emailUserIdMap.get(email)== null) {
                    emailUserIdMap.put(email, id);
                }
            }

        });
        return emailUserIdMap;
    }

    private static Optional<String> toUserEmailQuery(final List<String> emails ){
        if (emails.isEmpty())
            return Optional.empty();

        final StringBuilder query =  new StringBuilder();
        query.append("select Id,SenderEmail, Email ").append(" from User where ").append(" SenderEmail in ( ");

        emails.forEach(email -> {
            query.append("\'").append(email).append("\' ,");
        });
        query.deleteCharAt(query.toString().length()-1);
        query.append(")").append(" or ").append(" Email in ( ");
        emails.forEach(email -> {
            query.append("\'").append(email).append("\' ,");
        });
        query.deleteCharAt(query.toString().length()-1);
        query.append(")");
        LOG.debug("User email query: "+query.toString());

        return Optional.of(query.toString());

    }
}
