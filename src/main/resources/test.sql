CREATE DATABASE IF NOT EXISTS `homepassintegration`;
USE `homepassintegration`;

CREATE TABLE IF NOT EXISTS `organisation` (
  `id` varchar(50) NOT NULL,
  `namespace` varchar(10) NOT NULL
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `office` (
  `org_id` varchar(50) NOT NULL,
  `office_id` varchar(50) NOT NULL
  PRIMARY KEY (`org_id`)
) ENGINE=InnoDB;


CREATE TABLE IF NOT EXISTS `homepass_apisecret` (
  `org_id` varchar(50) NOT NULL,
  `secret` varchar(100) NOT NULL,
  PRIMARY KEY (`org_id`)
) ENGINE=InnoDB;


CREATE TABLE `homepass_integration_request` (
  `org_id` varchar(50) NOT NULL,
  `namespace` varchar(50) NOT NULL,
  PRIMARY KEY (`org_id`)
) ENGINE=InnoDB;

CREATE TABLE `integration_office` (
  `office_id` varchar(50) NOT NULL,
  `org_id` varchar(50) NOT NULL,
  `project` BOOLEAN NOT NULL,
  PRIMARY KEY (`office_id`),
  KEY `org_id` (`org_id`),
  CONSTRAINT `integration_office_fk` FOREIGN KEY (`org_id`) REFERENCES `homepass_integration_request` (`org_id`)
) ENGINE=InnoDB;



CREATE TABLE `office_synchronise_datetime` (
  `org_id` varchar(50) NOT NULL,
  `office_id` varchar(50) NOT NULL,
  `last_synchronise_datetime` varchar(50) NOT NULL,
  PRIMARY KEY (`org_id`, `office_id`)
) ENGINE=InnoDB;




CREATE TABLE `HomepassProcessIndex` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL,
  `lastProcItem` int(11) DEFAULT NULL,
  `lastProcPage` int(11) DEFAULT NULL,
  `officeId` varchar(255) NOT NULL,
  `orgId` varchar(60) NOT NULL,
  `procWholePage` bit(1) DEFAULT NULL,
  `lastActivityDatetime` varchar(50) DEFAULT NULL,
  `lastActivityId` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_ORG_Id` (`orgId`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=latin1


